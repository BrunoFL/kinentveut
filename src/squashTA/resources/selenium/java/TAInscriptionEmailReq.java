import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.fail;

public class TAInscriptionEmailReq extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 15000)
    public void testTAInscriptionEmailReq() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='send'])[1]/following::h4[1]")).click();
        Assert.assertEquals("true", driver.findElement(By.id("email-inscription")).getAttribute("required"));
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
