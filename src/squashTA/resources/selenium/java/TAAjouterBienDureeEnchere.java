import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TAAjouterBienDureeEnchere extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 180000)
    public void testTAAjouterBienDureeEnchere() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
//        System.out.println("Page get and click ok");
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Ami");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("ami");
        driver.findElement(By.id("submit-connexion")).click();
//        System.out.println("Auth");
        sleep(1000);
        driver.findElement(By.id("add_bien_form")).click();
//        System.out.println("Add bien click, filling values");
        sleep(500);
        driver.findElement(By.id("ajout-bien-prix-base")).clear();
        driver.findElement(By.id("ajout-bien-prix-base")).sendKeys("20");
        driver.findElement(By.id("ajout-bien-prix-reserve")).clear();
        driver.findElement(By.id("ajout-bien-prix-reserve")).sendKeys("30");
        driver.findElement(By.id("ajout-bien-libelle")).clear();
        driver.findElement(By.id("ajout-bien-libelle")).sendKeys("TestTemps");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Calendar cal = Calendar.getInstance();
//        System.out.println(dateFormat.format(cal.getTime()));
//        System.out.println(timeFormat.format(cal.getTime()));
        driver.findElement(By.id("ajout-bien-date")).clear();
        driver.findElement(By.id("ajout-bien-date")).sendKeys(dateFormat.format(cal.getTime()));
        cal.add(Calendar.MINUTE, 1);
        driver.findElement(By.id("ajout-bien-heure")).clear();
        driver.findElement(By.id("ajout-bien-heure")).sendKeys(timeFormat.format(cal.getTime()));
//        System.out.println("Values filled");
        driver.findElement(By.id("submit-ajout-bien")).click();
        sleep(62000);
//        System.out.println("Searching for new item timedout");
        Assert.assertEquals("Vous n'avez pas été suffisament rapide !", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TestTemps'])[1]/following::div[6]")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
