import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TAEncherirOk extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 60000)
    public void testTAEncherirOk() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Toto");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("TitiToto");
        driver.findElement(By.id("submit-connexion")).click();
        sleep(500);
//        System.out.println("Auth toto");

        driver.findElement(By.id("add_bien_form")).click();
        driver.findElement(By.id("ajout-bien-prix-base")).clear();
        driver.findElement(By.id("ajout-bien-prix-base")).sendKeys("70");
        driver.findElement(By.id("ajout-bien-prix-reserve")).clear();
        driver.findElement(By.id("ajout-bien-prix-reserve")).sendKeys("80");
        driver.findElement(By.id("ajout-bien-libelle")).clear();
        driver.findElement(By.id("ajout-bien-libelle")).sendKeys("VTT");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Calendar cal = Calendar.getInstance();
        driver.findElement(By.id("ajout-bien-date")).clear();
        driver.findElement(By.id("ajout-bien-date")).sendKeys(dateFormat.format(cal.getTime()));
        cal.add(Calendar.MINUTE, 15);
        driver.findElement(By.id("ajout-bien-heure")).clear();
        driver.findElement(By.id("ajout-bien-heure")).sendKeys(timeFormat.format(cal.getTime()));
        driver.findElement(By.id("submit-ajout-bien")).click();
        sleep(500);
//        System.out.println("Adding Bien");

        driver.findElement(By.id("deconnexion")).click();
        sleep(500);
//        System.out.println("disconnected");

        driver.findElement(By.id("mon-compte")).click();
        sleep(500);

        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Ami");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("ami");
        driver.findElement(By.id("submit-connexion")).click();
        sleep(500);
//        System.out.println("Auth Ami");
//        System.out.println(driver.getPageSource());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='VTT'])[1]/following::i[1]")).click();
        driver.findElement(By.id("enchere-valeur")).clear();
        driver.findElement(By.id("enchere-valeur")).sendKeys("80");
        driver.findElement(By.id("submit-encherir")).click();
        sleep(500);
//        System.out.println("Enchere");

        Assert.assertEquals("80€", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='VTT'])[1]/following::span[2]")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
