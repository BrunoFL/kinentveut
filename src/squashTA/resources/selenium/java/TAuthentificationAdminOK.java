import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static java.lang.Thread.sleep;

public class TAuthentificationAdminOK extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 15000)
    public void testTAuthentificationAdminOK() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).sendKeys("admin");
        driver.findElement(By.id("password-connexion")).sendKeys("admin");
        driver.findElement(By.id("submit-connexion")).click();
        sleep(1000);
        Assert.assertEquals("ADMIN", driver.findElement(By.id("pseudo-utilisateur-co")).getText().toUpperCase());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
