import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TAAjouterBienPrixReserveInvalide extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 30000)
    public void testTAAjouterBienPrixReserveInvalide() throws Exception {
        driver.get(baseUrl);
//        System.out.println("Page get");
        driver.findElement(By.id("mon-compte")).click();
//        System.out.println("Mon compte");
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Ami");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("ami");
        driver.findElement(By.id("submit-connexion")).click();
//        System.out.println("Auth");
        sleep(1000);
        driver.findElement(By.id("add_bien_form")).click();
//        System.out.println("Add Click");
        driver.findElement(By.id("ajout-bien-prix-base")).clear();
        driver.findElement(By.id("ajout-bien-prix-base")).sendKeys("100");
        driver.findElement(By.id("ajout-bien-prix-reserve")).clear();
        driver.findElement(By.id("ajout-bien-prix-reserve")).sendKeys("80");
        driver.findElement(By.id("ajout-bien-libelle")).clear();
        driver.findElement(By.id("ajout-bien-libelle")).sendKeys("VTT");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        Calendar cal = Calendar.getInstance();
        driver.findElement(By.id("ajout-bien-date")).clear();
        driver.findElement(By.id("ajout-bien-date")).sendKeys(dateFormat.format(cal.getTime()));
        cal.add(Calendar.MINUTE, 2);
        driver.findElement(By.id("ajout-bien-heure")).clear();
        driver.findElement(By.id("ajout-bien-heure")).sendKeys(timeFormat.format(cal.getTime()));
//        System.out.println("Values filled");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Description'])[1]/following::i[1]")).click();
//        System.out.println("Send request");
        sleep(1000);
        Assert.assertEquals("le prix de réserve doit être superieur au de prix de base .", driver.findElement(By.id("helper-ajout-bien-reserve")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
