import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static java.lang.Thread.sleep;

public class TAuthenficationOk extends TestBase {
  @Before
  public void setUp() throws Exception {
    super.setUp();
  }

  @Test(timeout = 30000)
  public void testTAuthenficationOk() throws Exception {
    driver.get(baseUrl);
//    System.out.println("Page get ok");
//    System.out.println(driver.getPageSource());
    driver.findElement(By.id("mon-compte")).click();
//    System.out.println("Click mon compte ok");
    sleep(500);
//    System.out.println("Ecriture pseudo");
    driver.findElement(By.id("identifiant-connexion")).clear();
    driver.findElement(By.id("identifiant-connexion")).sendKeys("Toto");
//    System.out.println("Ecriture MDP");
    driver.findElement(By.id("password-connexion")).clear();
    driver.findElement(By.id("password-connexion")).sendKeys("TitiToto");
//    System.out.println("Envoie formulaire");
    driver.findElement(By.id("submit-connexion")).click();
    sleep(1000);
//    System.out.println("Asserting");
    Assert.assertEquals("TOTO", driver.findElement(By.id("pseudo-utilisateur-co")).getText().toUpperCase());
  }

  @After
  public void tearDown() throws Exception {
    super.tearDown();
  }
}
