import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TModifMDPKo1 extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 30000)
    public void testTModifMDPKo1() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Toto");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("TitiToto");
        driver.findElement(By.id("submit-connexion")).click();
        sleep(500);

        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.id("amdp-mon-compte")).sendKeys("TitiToto");
        driver.findElement(By.id("nmdp-mon-compte")).sendKeys("bonjour");
        driver.findElement(By.id("submit-mon-compte")).click();
        sleep(500);
        Assert.assertEquals("Mdp invalide", driver.findElement(By.id("helper-nmdp-mon-compte")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
