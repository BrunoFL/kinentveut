import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TModifMDPOk extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 60000)
    public void testTModifMDPOk() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);

        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='send'])[1]/following::h4[1]")).click();
        driver.findElement(By.id("email-inscription")).sendKeys("testModifMDPOk@gmail.com");
        driver.findElement(By.id("pseudo-inscription")).sendKeys("testModif");
        driver.findElement(By.id("password-inscription")).sendKeys("TestM2Info");
        driver.findElement(By.name("action")).click();
        sleep(500);

        driver.findElement(By.id("amdp-mon-compte")).clear();
        driver.findElement(By.id("amdp-mon-compte")).sendKeys("TestM2Info");
        driver.findElement(By.id("nmdp-mon-compte")).clear();
        driver.findElement(By.id("nmdp-mon-compte")).sendKeys("TestCgtMdp25");
        sleep(500);

        driver.findElement(By.id("deconnexion")).click();
        sleep(500);

        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("testModif");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("TestCgtMdp25");

        Assert.assertEquals("TESTMODIF", driver.findElement(By.id("pseudo-utilisateur-co")).getText().toUpperCase());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
