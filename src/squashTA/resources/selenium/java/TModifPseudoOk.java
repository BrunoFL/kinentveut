import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TModifPseudoOk extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 30000)
    public void testTModifPseudoOk() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='send'])[1]/following::h4[1]")).click();
        driver.findElement(By.id("email-inscription")).clear();
        driver.findElement(By.id("email-inscription")).sendKeys("test@test.com");
        driver.findElement(By.id("pseudo-inscription")).clear();
        driver.findElement(By.id("pseudo-inscription")).sendKeys("TestPseudo");
        driver.findElement(By.id("password-inscription")).clear();
        driver.findElement(By.id("password-inscription")).sendKeys("Pseudo25");
        driver.findElement(By.name("action")).click();
        sleep(500);
        driver.findElement(By.id("pseudo-utilisateur-co")).click();
        driver.findElement(By.id("pseudo-mon-compte")).clear();
        driver.findElement(By.id("pseudo-mon-compte")).sendKeys("Test");
        driver.findElement(By.id("amdp-mon-compte")).clear();
        driver.findElement(By.id("amdp-mon-compte")).sendKeys("Pseudo25");
        driver.findElement(By.id("submit-mon-compte")).click();
        sleep(500);
        Assert.assertEquals("TEST", driver.findElement(By.id("pseudo-utilisateur-co")).getText().toUpperCase());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
