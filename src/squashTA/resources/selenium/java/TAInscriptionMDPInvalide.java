import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TAInscriptionMDPInvalide extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 15000)
    public void testTAInscriptionMDPInvalide() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        sleep(500);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='send'])[1]/following::h4[1]")).click();
        driver.findElement(By.id("email-inscription")).sendKeys("testm2@gmail.com");
        driver.findElement(By.id("pseudo-inscription")).sendKeys("toto2");
        driver.findElement(By.id("password-inscription")).sendKeys("test");
        driver.findElement(By.name("action")).click();
        sleep(1000);
        Assert.assertEquals("Mdp invalide", driver.findElement(By.id("helper-mdp-inscription")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
