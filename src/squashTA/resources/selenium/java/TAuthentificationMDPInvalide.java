import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.Thread.sleep;
import static org.junit.Assert.fail;

public class TAuthentificationMDPInvalide extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 15000)
    public void testTAuthentificationMDPInvalide() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        driver.findElement(By.id("identifiant-connexion")).click();
        driver.findElement(By.id("identifiant-connexion")).clear();
        driver.findElement(By.id("identifiant-connexion")).sendKeys("Toto");
        driver.findElement(By.id("password-connexion")).clear();
        driver.findElement(By.id("password-connexion")).sendKeys("TataToto");
        driver.findElement(By.id("submit-connexion")).click();
        sleep(1000);
        Assert.assertEquals("Mauvais mot de passe", driver.findElement(By.id("helper-mdp-connexion")).getText());
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
