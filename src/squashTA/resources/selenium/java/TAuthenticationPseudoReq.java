import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.fail;

public class TAuthenticationPseudoReq extends TestBase {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test(timeout = 15000)
    public void testTAuthenticationPseudoReq() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("mon-compte")).click();
        Assert.assertEquals("true", driver.findElement(By.id("identifiant-connexion")).getAttribute("required"));
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}
