<?php
//Verifie q'un mot de pass est bien formé
namespace UniTestor{

    class VerificateurMDP{
        private $mdp;

        public function __construct($mdp){
            $this->mdp = $mdp;
        }

        public function isValid(){
            return $this->hasLengthMinimum(8) &&
                    $this->hasLetterMaj() &&
                    $this->hasLetterMin() &&
                    $this->hasNumber();
        }

        public function hasLengthMinimum($length){
            return strlen($this->mdp) >= $length;
        }

        public function hasNumber(){
            return preg_match("/[0-9]/", $this->mdp);
        }

        public function hasLetterMin(){
            return preg_match("/[a-z]/", $this->mdp);
        }

        public function hasLetterMaj(){
            return preg_match("/[A-Z]/", $this->mdp);
        }
    }
}