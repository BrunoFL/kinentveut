<?php
require_once ('MYPDO.php');
require_once ('UtilsDB.php');
require_once ('Connexion.php');
require_once ('Inscription.php');
require_once ('Categories.php');
require_once ('AjoutBien.php');
require_once ('AjoutEnchere.php');
require_once ('Enchere.php');
require_once ('Navbar.php');
require_once ('MonCompteForm.php');
require_once ('MonCompte.php');
require_once ('AdminForms.php');
require_once ('ModificationBien.php');
require_once ('ModificationUtilisateurs.php');
require_once ('ModificationCategorie.php');
require_once ('RequeteAjax.php');
require_once ('Recherche.php');

session_start();
$pdo = new \UniTestor\MYPDO();
$util = new \UniTestor\UtilsDB($pdo);
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

switch ($_POST['func']) {
    case 'connexion':
        $c = new \UniTestor\Connexion($util);
        $c->connexion();
        echo $c->getReturnJSON();
        break;

    case 'inscription':
        $i = new \UniTestor\Inscription($util);
        $i->inscription();
        echo $i->getReturnJSON();
        break;

    case 'categories':
        $c = new \UniTestor\Categories($util);
        $c->getCategories();
        echo $c->getReturn();
        break;

    case 'ajoutBien':
        $a = new \UniTestor\AjoutBien($util);
        $a->ajoutBien();
        echo $a->getReturnJSON();
        break;

    case 'encherir':
        $a = new \UniTestor\AjoutEnchere($util);
        $a->encherir();
        echo $a->getReturnJSON();
        break;

    case 'getNavbar':
        echo getNavBar();
        break;

    case 'getMonCompteForm':
        echo getMonCompteForm();
        break;

    case 'updateMonCompte':
        $m = new \UniTestor\MonCompte($util);
        $m->updateMonCompte();
        echo $m->getReturnJSON();
        break;

    case 'encheres':
        $e = new \UniTestor\Enchere($util);
        $e->getEnchereListe();
        echo $e->getReturnJSON();
        break;

    case 'AdminBiens':
        $a = new \UniTestor\AdminForms($util);
        $a->getAdminFormBiens();
        echo $a->getReturnJSON();
        break;

    case 'AdminUsers':
        $a = new \UniTestor\AdminForms($util);
        $a->getAdminFormUsers();
        echo $a->getReturnJSON();
        break;

    case 'AdminCate':
        $a = new \UniTestor\AdminForms($util);
        $a->getAdminFormCategories();
        echo $a->getReturnJSON();
        break;

    case 'adminUpdateUser':
        $m = new \UniTestor\ModificationUtilisateurs($util);
        $m->modifUtilisateur();
        echo $m->getReturnJSON();
        break;

    case 'adminUpdateCate':
        $m = new \UniTestor\ModificationCategorie($util);
        $m->modifCategorie();
        echo $m->getReturnJSON();
        break;

    case 'adminAddCate':
        $m = new \UniTestor\ModificationCategorie($util);
        $m->addCategorie();
        echo $m->getReturnJSON();
        break;

    case 'adminDeleteCate':
        $m = new \UniTestor\ModificationCategorie($util);
        $m->deleteCategorie();
        echo $m->getReturnJSON();
        break;

    case 'adminUpdateBien':
        $m = new \UniTestor\ModificationBien($util);
        $m->modifEnchere();
        echo $m->getReturnJSON();
        break;

    case 'search':
        $s = new \UniTestor\Recherche($util);
        $s->recherche();
        echo $s->getReturnJSON();
        break;

    default:
        echo json_encode('Erreur, requete non prise en charge');
    break;
}
