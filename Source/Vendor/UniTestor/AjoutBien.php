<?php
//Gere l'ajout d'un nouveau bien
namespace UniTestor{
    require_once ('RequeteAjax.php');

    class AjoutBien extends \RequeteAjax{
        private $pseudo;
        private $prix_base;
        private $prix_reserve;
        private $libelle;
        private $description;
        private $confidentialite;
        private $date;
        private $user;
        private $userID;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        public function ajoutBien(){
            $this->getParams();
            if ($this->dateIsNotValid()){
                $this->return['code'] = 3;
                $this->return['msg'] = 'date de fin invalide';
                return;
            }            

            if ($this->priceIsNotValid()){
                $this->return['code'] = 4;
                $this->return['msg'] = 'le prix de réserve doit être superieur au de  prix de base .';
                return;    
            }

            $val = $this->addBien($this->userID);
            if ($val != 0){
                $this->return['code'] = 0;
                $this->return['msg'] = "Bien ajouté !";
                return;
            }
        }

        private function getParams(){
            $this->pseudo = (isset($_SESSION['pseudo'])) ? $_SESSION['pseudo'] : "invite";
            $this->prix_base = isset($_POST['prix_base']) ?$_POST['prix_base'] : NULL;
            $this->prix_reserve = isset($_POST['prix_reserve'])?$_POST['prix_reserve'] : NULL;
            $this->libelle =isset($_POST['libelle']) ? $_POST['libelle'] : NULL;
            $this->description = isset( $_POST['description'])?$_POST['description'] : NULL;
            $this->confidentialite = isset($_POST['confidentialite'])?$_POST['confidentialite'] : NULL;
            $this->date = isset($_POST['date']) ? $_POST['date'] : "now" ;
            $this->userID = $_SESSION['userID'];
        }

        private function dateIsNotValid(){
            $dateNow = new \DateTime("now");
            $dateFin = new \DateTime($this->date);
            return $dateNow >= $dateFin;
        }

        private function priceIsNotValid(){
            return $this->prix_reserve <= $this->prix_base;
        }

        public function addBien($user){
            return $this->util->addBien(array($user,$this->libelle,$this->description, $this->confidentialite, $this->prix_base, $this->prix_reserve,$this->date));
        }
        
        public function getId(){
            return $this->util->getIdByPseudo(array($this->pseudo));
        }        
    }
}