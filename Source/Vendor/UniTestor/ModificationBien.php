<?php
//Gere l'ajout d'une enchere
namespace UniTestor{
    require_once('RequeteAjax.php');

    class ModificationBien extends \RequeteAjax{
        private $id_user;
        private $id_bien;
        private $newState;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }


        public function editStatutEnchere(){
            return $this->util->editStatutEnchere(array($this->newState, $this->id_bien));
        }

        public function modifEnchere(){
            //l'utilisateur doit être un admin
            if($_SESSION['statut']!='admin'){
                $this->return['code'] = 2;
                $this->return['msg'] = 'L utilisateur n est pas un administrateur';
                return;
            }
        
            $this->id_bien = $_POST['id_bien'];
            $this->newState = $_POST['etat'];
            $res = $this->editStatutEnchere();
            
            if($res == 1){
                $this->return['code'] = 0;
                $this->return['msg'] = 'Bien modifié';
            }

            return;
        }
    }
}
