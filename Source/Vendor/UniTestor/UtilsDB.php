<?php

namespace UniTestor{
    require_once ('MYPDO.php');
    class UtilsDB{
        private $db;

        public function __construct($db){
            $this->db = $db;
        }

        public function getLastId(){
            return intVal($this->db->lastInsertId());
        }

        public function getIdentifiantByMailOrPseudo($array){
            $res = $this->db->row("SELECT email,pseudo, statut, id FROM utilisateur WHERE pseudo = ? OR email = ?", $array);
            return $res;
        }

        public function getPasswordByMailOrPseudo($array){
            $res = $this->db->row("SELECT psswd FROM utilisateur WHERE pseudo = ? OR email = ?", $array);
            return  $res['psswd'];
        }

        public function getPseudoByPseudo($array){
            return $this->db->row("SELECT pseudo FROM utilisateur WHERE pseudo = ?",$array);
        }

        public function getPseudoByEmail($array){
            return $this->db->row("SELECT pseudo FROM utilisateur WHERE email = ?", $array);
        }

        public function insertUser($array){
            return $this->db->query("INSERT INTO utilisateur(pseudo,psswd,email) VALUES(?,?,?)", $array);
        }

        public function getCategories(){
            return $this->db->query("SELECT * FROM categorie");
        }

        public function getIdByPseudo($array){
            return $this->db->row("SELECT id FROM utilisateur WHERE pseudo = ?", $array);
        }

        public function addBien($array){
            return $this->db->query("INSERT INTO bien(id_user,libelle,description,visibilite, prix_base,prix_reserve,date_parution,duree,etat,categorie)
            VALUES(?,?,?,?,?,?,NOW(),?,'accepte',1)", $array);
        }

        public function addCategorie($array){
            return $this->db->query("INSERT INTO categorie(libelle) VALUES (?)", $array);
        }

        public function addEnchere($array){
            return $this->db->query("INSERT INTO enchere (id_bien,id_user,montant) VALUES(?,?,?)", $array);
        }

        public function getUserByMail($array){
            return $this->db->row("SELECT * FROM utilisateur WHERE email = ?", $array);
        }

        public function getUserByPseudo($array){
            return $this->db->row("SELECT * FROM utilisateur WHERE pseudo = ?", $array);
        }

        public function updatePseudoUserByMail($array){
            return $this->db->query("UPDATE utilisateur SET pseudo = ? WHERE email = ?", $array);
        }

        public function updateMDPUserByMail($array){
            return $this->db->query("UPDATE utilisateur SET psswd = ? WHERE email = ?", $array);
        }

        public function getAllBiens(){
            return $this->db->query("SELECT * FROM bien");
        }
        public function getAllUsers(){
            return $this->db->query("SELECT * FROM utilisateur");
        }

        public function getEnchereList($params){
            $select = "SELECT b.libelle, b.description, b.id, b.duree, GREATEST(b.prix_base, IF(e.maxMontant is null, 0, e.maxMontant)) AS prix_courant";

            $whereSubrequest = "WHERE 1";

            $where = "WHERE etat = 'accepte'";

            $order = "";

            $array = array();

            // Mes biens
            if ($_POST['mes_encheres'] == 'true') {
                $where .= " AND b.id_user = ?";
                array_push($array, $params["userId"]);
            }

            // Mes encheres
            if ($_POST['deja_encheri'] == 'true') {
                $whereSubrequest .= " AND e.id_user = ?";
                array_push($array, $params["userId"]);
            }

            // Seulement_en_cours
            if ($_POST['seulement_en_cours'] == 'true') {
                $where .= " AND duree > NOW()";
            } else {
                $where .= " AND duree < NOW()";
            }

            // Ordre
            if ($_POST['tri'] == 'tri-croissant') {
                $order .= "ORDER BY prix_courant";
            }
            if ($_POST['tri'] == 'tri-decroissant') {
                $order .= "ORDER BY prix_courant DESC";
            }
            if ($_POST['tri'] == 'temps') {
                $order .= "ORDER BY duree DESC";
            }

            // Catégories
            if (isset($_POST['categorie'])) {
                $where .= " AND categorie IN (";
                foreach ($_POST['categorie'] as $idCat) {
                    $where .= "?,";
                    array_push($array, $idCat);
                }
                $where .= "0)";
            }

            // Assemblement from
            $from = "FROM bien b " .
                ($_POST['deja_encheri'] == 'true' ? "INNER" : "LEFT") .
                " JOIN (SELECT e.id_bien, MAX(e.montant) AS maxMontant
                    FROM enchere e " . $whereSubrequest . " GROUP BY e.id_bien) e
                ON b.id = e.id_bien";

            // Confidentialité
            if ($_POST['confidentialite'] == 'public' || empty($_SESSION)) {
                $where .= " AND visibilite = 'public'";
            } else if ($_POST['confidentialite'] == 'privee') {
                $where .= " AND (visibilite = 'privee' OR visibilite = 'public')";
            } else if ($_POST['confidentialite'] == 'confidentielle') {
                $where .= " AND visibilite = 'confidentielle'
                    AND (li.id_user = ? OR b.id_user = ?)";
                array_push($array, $params["userId"]);
                array_push($array, $params["userId"]);
                $from .= " LEFT JOIN liste_confidentielle li ON b.id = li.id_bien";
            }

            $request = $select." ".$from." ".$where." ".$order.";";

            // echo($request);
            // echo($array);
            return $this->db->query($request, $array);
        }

        public function getEnchereRecherche($params){
            return $this->db->query("SELECT b.libelle, b.description, b.id, b.duree, GREATEST(b.prix_base, IF(e.maxMontant is null, 0, e.maxMontant)) AS prix_courant FROM bien b LEFT JOIN (SELECT e.id_bien, MAX(e.montant) AS maxMontant FROM enchere e GROUP BY e.id_bien) e ON b.id = e.id_bien WHERE etat = 'accepte' AND libelle LIKE '%" . $params[0] . "%'");
        }

        public function getBienById($array){
            return $this->db->row("SELECT * FROM bien WHERE id = ?", $array);
        }

        public function getLastEnchereById($array){
            return $this->db->row("SELECT GREATEST(b.prix_base, IF(e.maxMontant is null, 0, e.maxMontant)) AS prix_courant FROM bien b LEFT JOIN (SELECT e.id_bien, MAX(e.montant) AS maxMontant FROM enchere e WHERE 1 GROUP BY e.id_bien) e ON e.id_bien = b.id WHERE b.id = ?", $array);
        }

        public function editStatutEnchere($array){
            return $this->db->query("UPDATE bien SET etat = ? WHERE id = ?", $array);
        }

        public  function modifUserStatut($array){
            return $this->db->query("UPDATE utilisateur SET statut = ? WHERE id = ?" , $array);
        }

        public function modifCategorie($array){
            return $this->db->query("UPDATE categorie SET libelle = ? WHERE id = ?", $array);
        }

        public  function deleteCategorie($array){
            return $this->db->query("DELETE FROM categorie WHERE id = ?", $array);
        }

    }
}
