<?php
//Retourne la barre de navigation

function getNavbar(){

    $dropdown = '';
    $res ='<nav class="light-blue lighten-1" role="navigation">'.
    '<div class="nav-wrapper container">'.
    '<a id="logo-container" href="#" class="brand-logo">Qui n\'en veut</a>'.
    '<ul class="right">';
    
    if(isset($_SESSION['pseudo'])){
        $res .= '<li><a id="deconnexion" class="waves-effect waves-white btn-large red" href="deco.php">'.
        '<i class="material-icons right">account_circle</i>'.
        'Deconnexion</a></li>';
        
        
        $res .= '<li><a id="mon-compte" class="waves-effect waves-white btn-large modal-trigger" data-target="modal1-mon-compte">'.
        '<i class="material-icons right">settings</i><span id="pseudo-utilisateur-co">'.
        htmlspecialchars($_SESSION['pseudo'], ENT_QUOTES, 'UTF-8').
        '</span></a></li>';


        if ($_SESSION['statut'] == 'admin'){
            $res .= '<li><a id="administration" class="waves-effect waves-white btn-large orange dropdown-trigger" data-target="dropdown-admin">'.
            '<i class="material-icons right">arrow_drop_down</i>'.
            'Administration</a></li>';
            $dropdown = '<ul id="dropdown-admin" class="dropdown-content">'.
            '<li><a id="admin-btn-modale-biens" class="btn-large orange waves-effect waves-white modal-trigger white-text" data-target="modale-admin-biens">Les biens</a></li>'.
            '<li><a id="admin-btn-modale-users" class="btn-large orange waves-effect waves-white modal-trigger white-text" data-target="modale-admin-users">Les utilisateurs</a></li>'.
            '<li><a id="admin-btn-modale-cate" class="btn-large orange waves-effect waves-white modal-trigger white-text" data-target="modale-admin-cate">Les catégories</a></li></ul>';
        }
    }else{
        $res .='<li><a id="mon-compte" class="waves-effect waves-teal btn-large modal-trigger" data-target="modal1-connexion">'.
        '<i class="material-icons right">account_circle</i>Connexion Inscription'.
        '</a></li>';
    }
    
    
    $res .= '</ul></div></nav>'.$dropdown;
    return $res;
}
