<?php
//Et gere le formulaire
namespace UniTestor{
    require_once ('RequeteAjax.php');

    class MonCompte extends \RequeteAjax{
        private $mail;
        private $pseudo;
        private $amdp;
        private $nmdp;
        private $user;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->return['msgs'] = array(); 
            $this->util = $utils;
        }

        private function getParams(){
            $this->mail = $_POST['mail'];
            $this->pseudo = $_POST['pseudo'];
            $this->amdp = $_POST['amdp'];
            $this->nmdp = (isset($_POST['nmdp']) && $_POST['nmdp'] != "") ? $_POST['nmdp'] : null;
        }

        private function mdpIsNotCorrect(){
            return (!password_verify($this->amdp, $this->user['psswd']));
        }

        private function pseudoHasChanged(){
            return $_SESSION['pseudo'] !== $this->pseudo;
        }

        private function pseudoLookLikeMail(){
            return ("" != filter_var($this->pseudo, FILTER_VALIDATE_EMAIL));
        }

        private function mdpIsNotValid(){
            $verifMDP = new \UniTestor\VerificateurMDP($this->nmdp);
            return (!$verifMDP->isValid());
        }

        public function getUser(){
            return $this->util->getUserByMail(array($this->mail));
        }

        public function updatePseudo(){
            return $this->util->updatePseudoUserByMail(array($this->pseudo, $this->mail));
        }

        public function updateMDP(){
            $hash = password_hash($this->nmdp, PASSWORD_DEFAULT);
            return $this->util->updateMDPUserByMail(array($hash, $this->mail));
        }

        public function pseudoNonLibre(){
            $res = $this->util->getUserByPseudo(array($this->nmdp));
            return isset($res['pseudo']);
        }

        //Modification du compte
        public function updateMonCompte(){
            $this->getParams();

            //Verificatipn mdp correcte
            $this->user = $this->getUser();
            if ($this->mdpIsNotCorrect()){
                $this->return['code'] = 4;
                $this->return['msg'] = "Mot de passe incorrect";
                return;
            }

            //Changement pseudo
            if ($this->pseudoHasChanged()){
                if ($this->pseudoLookLikeMail()){
                    $this->return['code'] = 7;
                    $this->return['msg'] = 'Pas de mail';
                    return;    
                }
                if ($this->pseudoNonLibre()){
                    $this->return['code'] = 5;
                    $this->return['msg'] = "Pseudo non disponible";
                    return;
                }
                $res = $this->updatePseudo();
                if ($res == 1){
                    $this->return['code'] = 2;
                    $this->return['msg'] = "";
                    array_push($this->return['msgs'], "Changement de pseudo effectué");
                    $_SESSION['pseudo'] = $this->pseudo;
                }   
            }

            if (!is_null($this->nmdp)){
                if ($this->mdpIsNotValid()){
                    $this->return['code'] = 6;
                    $this->return['msg'] = 'Mdp invalide';
                    return;
                }
                
                $res = $this->updateMDP();
                if ($res == 1){
                    $this->return['code'] = 3;
                    array_push($this->return['msgs'], "Changement de mot de passe effectué");
                }
            }
        }
    }
}
    