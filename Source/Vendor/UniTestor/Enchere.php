<?php
// Retourne la liste des encheres
namespace UniTestor{
    require_once ('RequeteAjax.php');

    class Enchere extends \RequeteAjax {
        private $userId;

        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }
        
        public function getEnchereListe(){
            $this->userId = (isset($_SESSION['userID']) && !empty($_SESSION['userID']) ? intval($_SESSION['userID']) : 4);
            $this->return = $this->util->getEnchereList(array("userId" => $this->userId));
        }
    }
}
