<?php
//Gere l'inscription d'un nouvel utilisateur
namespace UniTestor{
    require_once ('RequeteAjax.php');
    require_once ('VerificateurMDP.php');

    class Inscription extends \RequeteAjax{
        private $mail;
        private $pseudp;
        private $mdp;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        public function inscription(){
            $this->getParams();

            if ($this->pseudoIsAlreadyUsed()){
                $this->return['code'] = 2;
                $this->return['msg'] = 'Pseudo déjà utilisé';
                return;    
            }

            if ($this->pseudoLookLikeMail()){
                $this->return['code'] = 6;
                $this->return['msg'] = 'Pseudo invalide (pas de mail)';
                return;
            }

            if ($this->mailIsAlreadyUsed()){
                $this->return['code'] = 3;
                $this->return['msg'] = 'Email déjà utilisé';
                return;
            }

            if ($this->mailIsNotValid()){
                $this->return['code'] = 4;
                $this->return['msg'] = 'Mail invalide';
                return;    
            }

            if ($this->mdpIsNotValid()){
                $this->return['code'] = 5;
                $this->return['msg'] = 'Mdp invalide';
                return;
            }
            
            $res = $this->addUser();
            if ($res == 1){
                $_SESSION['pseudo'] = $this->pseudo;
                $_SESSION['email'] =  $this->mail;
                $_SESSION['statut'] =  'user';
                $res = $this->util->getIdByPseudo(array($this->pseudo));
                $id = $res['id'];
                $_SESSION['userID'] = intval($id);
                $this->return['code'] = 0;
                $this->return['msg'] = "Bienvenue parmi nous !";
                
                return;
            }
        }

        private function getParams(){
            $this->mail = $_POST['mail'];
            $this->pseudo = $_POST['pseudo'];
            $this->mdp = $_POST['mdp'];
        }
        
        private function pseudoIsAlreadyUsed(){
            $res = $this->getPseudo();
            return $res > 0;
        }

        private function pseudoLookLikeMail(){
            return ("" != filter_var($this->pseudo, FILTER_VALIDATE_EMAIL));
        }

        private function mailIsAlreadyUsed(){
            $res = $this->getMail();
            return $res > 0;
        }

        private function mailIsNotValid(){
            return ($this->mail != filter_var($this->mail, FILTER_VALIDATE_EMAIL));
        }

        private function mdpIsNotValid(){
            $verifMDP = new \UniTestor\VerificateurMDP($this->mdp);
            return (!$verifMDP->isValid());
        }

        public function getPseudo(){
            return $this->util->getPseudoByPseudo(array($_POST['pseudo']));
        }

        public function getMail(){
           return $this->util->getPseudoByEmail(array($_POST['mail']));
        }

        public function addUser(){
            $hash = password_hash($this->mdp, PASSWORD_DEFAULT);
            return $this->util->insertUser(array($_POST['pseudo'], $hash, $_POST['mail']));
        }

    }
}