<?php
//Gere la connexion

namespace UniTestor {
    require_once ('RequeteAjax.php');

    class Connexion extends \RequeteAjax{
        private $identifiant;
        private $id;
        private $password;
        private $user;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }
    
        public function connexion(){
            if ($this->getParams()){
                $this->return['msg'] = "Pas de saisie";
                $this->return['code'] = 5;
                return;
            }
            
            if ($this->identifiantNotExists()){
                $this->return['msg'] = "Identifiant introuvable";
                $this->return['code'] = 1;
                return;
            }
            
            if ($this->passwordIsNotValid()){
                $this->return['msg'] = "Mauvais mot de passe";
                $this->return['code'] = 2;
                return;
            }

            $this->validAuth();
            $this->updateSession();            
        }

        private function getParams(){
            if(!isset($_POST['identifiant']) || !isset($_POST['password']))
                return false;

            $this->identifiant = $_POST['identifiant'];
            $this->password = $_POST['password'];

        }
        
        private function identifiantNotExists(){
            $this->user = $this->getIdentifiant();
            if ($this->user['pseudo'] == $this->identifiant || $this->user['email'] == $this->identifiant){
                $this->pseudo = $this->user['pseudo'];
                $this->email = $this->user['email'];
                return false;
            }
            return true;
        }

        private function passwordIsNotValid(){
            $res = $this->getPassword();
            return !password_verify($_POST['password'], $res);
        }

        private function validAuth(){
            $this->return['code'] = 0;
            $this->return['msg'] = "OK";
            $this->return['pseudo'] = htmlspecialchars($this->user['pseudo'], ENT_QUOTES, 'UTF-8');
        }

        private function updateSession(){
            $_SESSION['pseudo'] = $this->user['pseudo'];
            $_SESSION['email'] =  $this->user['email'];
            $_SESSION['statut'] =  $this->user['statut'];
            $_SESSION['userID'] = $this->user['id'];
        }

        public function getIdentifiant(){
            return $this->util->getIdentifiantByMailOrPseudo(array($_POST['identifiant'],$_POST['identifiant']));
        }

        public function getPassword(){
            return $this->util->getPasswordByMailOrPseudo(array($_POST['identifiant'],$_POST['identifiant']));
        }
        
    }
}