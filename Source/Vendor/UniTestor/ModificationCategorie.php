<?php
//Gere l'ajout d'une enchere
namespace UniTestor {
    require_once ('RequeteAjax.php');

    class ModificationCategorie extends \RequeteAjax{
        private $id_cate;
        private $libelle;


        public function __construct($utils)
        {
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        public function modifCategorie()
        {
            $this->id_cate = $_POST['id_cate'];
            $this->libelle = $_POST['libelle'];

            //l'utilisateur doit être un admin
            if ($_SESSION['statut'] != 'admin') {
                $this->return['code'] = 2;
                $this->return['msg'] = 'L utilisateur n est pas un administrateur';
                return;
            }


            if (empty($this->libelle)) {
                $this->return['code'] = 5;
                $this->return['msg'] = 'titre n est pas rempli';
                return;
            }

            if (strlen($this->libelle) >= 26) {
                $this->return['code'] = 6;
                $this->return['msg'] = 'titre trop long';
                return;

            }

            $res = $this->util->modifCategorie(array($this->libelle, $this->id_cate));
            if ($res == 1){
                $this->return['code'] = 0;
                $this->return['msg'] = "Catégorie modifiée";
            }

        }

        public function addCategorie(){
            $this->libelle = $_POST['libelle'];
            if ($_SESSION['statut'] != 'admin') {
                $this->return['code'] = 2;
                $this->return['msg'] = 'L utilisateur n est pas un administrateur';
                return;
            }

            $res = $this->util->addCategorie(array($this->libelle));
            if ($res == 1) {
                $this->return['code'] = 0;
                $this->return['msg'] = 'Catégorie ajoutée';
                return;
            }


        }

        public function deleteCategorie()
        {
            $this->id_cate = $_POST['id_cate'];

            if ($_SESSION['statut'] != 'admin') {
                $this->return['code'] = 2;
                $this->return['msg'] = 'L utilisateur n est pas un administrateur';
                return;
            }
            
            $res = $this->util->deleteCategorie(array($this->id_cate));
            if ($res == 1) {
                $this->return['code'] = 0;
                $this->return['msg'] = 'Catégorie supprimée';
                return;
            } else {
                $this->return['code'] = 3;
                $this->return['msg'] = "Erreur, la catégorie n'existe pas";
            }
            return;

        }
    }
}