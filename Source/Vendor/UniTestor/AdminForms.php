<?php
namespace UniTestor{
    require_once ('RequeteAjax.php');
    class AdminForms extends \RequeteAjax{
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        public function getAdminFormBiens(){
            $this->return['code'] = 0;
            $res = $this->util->getAllBiens();
            $return = array();
            foreach ($res as $indice => $resultat){
                $el = array();
                foreach($resultat as $key => $value){
                    $el[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                }
                $return[$indice] = $el;
            }
            $this->return['msg'] = $return;
        }

        public function getAdminFormUsers(){
            $this->return['code'] = 0;
            $res = $this->util->getAllUsers();
            $return = array();
            foreach ($res as $indice => $resultat){
                $el = array();
                foreach($resultat as $key => $value){
                    $el[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                }
                $return[$indice] = $el;
            }
            $this->return['msg'] = $return;
        }
        
        public function getAdminFormCategories(){
            $this->return['code'] = 0;
            $res = $this->util->getCategories();
            $return = array();
            foreach ($res as $indice => $resultat){
                $el = array();
                foreach($resultat as $key => $value){
                    $el[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                }
                $return[$indice] = $el;
            }
            $this->return['msg'] = $return;
        }

    }
}