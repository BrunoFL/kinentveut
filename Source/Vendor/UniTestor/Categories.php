<?php
//Retourne la liste des categories

namespace UniTestor{
    require_once('UtilsDB.php');

    class Categories{
        private $util;
        private $return;
        
        public function __construct($utils){
            $this->return = "";
            $this->util = $utils;
        }
        
        public function getCategories(){
            $res = $this->util->getCategories();
            if (!empty($res)){
                foreach ($res as $val){
                    $this->return .= '<option value="'.intval($val['id']).'">'.htmlspecialchars($val['libelle'], ENT_QUOTES, 'UTF-8').'</option>';
                }
            }
            return $this->return;
        }
        
        public function getReturn(){
            return $this->return;
        }

    }
    
}