/* Fichier JS -- Fonctionnement
Lors d'une soumission d'un formulaire ou d'un clic ou d'un changement (evenement)
Le code recupere les valeurs necessaires puis les transmets au fichier php qui convient
Le php renvoit du code HTML ou JSON qui est ensuite traité puis intégré à la page
https://developer.mozilla.org/fr/docs/Web/Guide/AJAX

*/
var x;

(function ($) {
    $(function () {//Attend que la page soit chargé

        //Initialise les elements de base
        init();

        // Formulaire Inscription AJAX
        $(document).on('submit', '#form-inscription', function (e) {
            e.preventDefault();
            $('#helper-mail-inscription').html('').removeClass('error-text');
            $('#helper-pseudo-inscription').html('').removeClass('error-text');
            $('#helper-mdp-inscription').html('').removeClass('error-text');
            $.post('Portail.php', {
                func: 'inscription',
                mail: $('#email-inscription').val(),
                pseudo: $('#pseudo-inscription').val(),
                mdp: $('#password-inscription').val()
            }, callbackInscription);
        });


        // Formulaire Connexion AJAX
        $(document).on('submit', '#form-connexion', function (e) {
            e.preventDefault();
            $('#helper-id-connexion').html('').removeClass('error-text');
            $('#helper-mdp-connexion').html('').removeClass('error-text');
            $.post('Portail.php',
                {
                    func: 'connexion',
                    identifiant: $('#identifiant-connexion').val(),
                    password: $('#password-connexion').val()
                }, callbackConnexion);
        });

        //Recupere formulaire modification du compte
        $(document).on('click', '#mon-compte', function (e) {
            refreshMonCompte();
        });

        // Formulaire Modification du compte AJAX
        $(document).on('submit', '#form-mon-compte', function (e) {
            e.preventDefault();
            $('#helper-nmdp-mon-compte').html('').removeClass('error-text');
            $('#helper-amdp-mon-compte').html('').removeClass('error-text');
            $('#helper-pseudo-mon-compte').html('').removeClass('error-text');
            $.post('Portail.php', {
                func: 'updateMonCompte',
                pseudo: $('#pseudo-mon-compte').val(),
                mail: $('#mail-mon-compte').val(),
                amdp: $('#amdp-mon-compte').val(),
                nmdp: $('#nmdp-mon-compte').val()
            }, callbackMonCompte);
        });

        // Construit la navbar
        refreshNavbar();
        //Met à jour les categories
        updateCategories();
        //Met a jour les biens
        updateBiens();
        // Recupere la liste des enchères
        $(document).on('change', '#edition', function (e) {
            updateBiens();
        });

        // Formulaire ajout d'un bien du compte AJAX
        $(document).on('submit', '#form-ajout-bien', function (e) {
            e.preventDefault();
            $('#helper-nmdp-mon-compte').html('').removeClass('error-text');
            $('#helper-amdp-mon-compte').html('').removeClass('error-text');
            $('#helper-pseudo-mon-compte').html('').removeClass('error-text');
            $.post('Portail.php', {
                func: 'ajoutBien',
                prix_base: $('#ajout-bien-prix-base').val(),
                prix_reserve: $('#ajout-bien-prix-reserve').val(),
                libelle: $('#ajout-bien-libelle').val(),
                description: $('#ajout-bien-description').val(),
                confidentialite: $('#ajout-bien-filtre-confidentialite').val(),
                date: $('#ajout-bien-date').val() + ' ' + $('#ajout-bien-heure').val() + ':00'
            }, callbackAjoutBien);
        });

        //Modifie l'id du formulaire modale pour encherir
        $(document).on('click', '#catalogue a', function (e) {
            var id = $(e.target).hasClass('btn-large') ? $(e.target).attr('data-id') : $(e.target).parent().attr('data-id');
            $('#enchere-id').html(id);

            var name;
            if ($(e.target).hasClass('btn-large')) {
                name = $(e.target).parent().parent().children().first().children().first().html();
            } else {
                name = $(e.target).parent().parent().parent().children().first().children().first().html();
            }
            $('#enchere-name').html(name);
        });

        // Formulaire Encherir du compte AJAX
        $(document).on('submit', '#form-encherir', function (e) {
            e.preventDefault();
            $.post('Portail.php', {
                func: 'encherir',
                val: $('#enchere-valeur').val(),
                id: $('#enchere-id').html()
            }, callbakEncherir);
        });

        // Formulaires de gestion administrateur
        $(document).on('click', '#admin-btn-modale-biens', function (e) {
            $.post('Portail.php', { func: 'AdminBiens' }, callbackAdminBiens);
        });
        $(document).on('click', '#admin-btn-modale-users', function (e) {
            $.post('Portail.php', { func: 'AdminUsers' }, callbackAdminUsers);
        });
        $(document).on('click', '#admin-btn-modale-cate', function (e) {
            $.post('Portail.php', { func: 'AdminCate' }, callbackAdminCate);
        });

        $(document).on('click', '#admin-update-user', function (e) {
            e.preventDefault();
            var id = $(e.target).attr('data-id');
            $.post('Portail.php', {
                func: 'adminUpdateUser',
                id_user: id,
                statut: $('#admin-update-user-select-id-' + id).val(),

            }, callbackFormAdminUser);
        });

        $(document).on('click', '#admin-update-bien', function (e) {
            e.preventDefault();
            var id = $(e.target).attr('data-id');
            $.post('Portail.php', {
                func: 'adminUpdateBien',
                id_bien: id,
                etat: $('#admin-update-bien-select-id-' + id).val(),

            }, callbackFormAdminBien);
        });

        $(document).on('click', '#admin-update-cate', function (e) {
            e.preventDefault();
            var id = $(e.target).attr('data-id');
            $.post('Portail.php', {
                func: 'adminUpdateCate',
                id_cate: id,
                libelle: $('#admin-update-cate-id-' + id).val(),

            }, callbackFormAdminCate);
        });

        $(document).on('click', '#admin-delete-cate', function (e) {
            e.preventDefault();
            var id = $(e.target).attr('data-id');
            $.post('Portail.php', {
                func: 'adminDeleteCate',
                id_cate: id
            }, callbackFormAdminCate);
        });

        $(document).on('click', '#admin-add-cate', function (e) {
            e.preventDefault();
            $.post('Portail.php', {
                func: 'adminAddCate',
                libelle: $('#admin-update-cat-input').val()
            }, callbackFormAdminCate);
        });

        $(document).on('input', '#recherche-text', function () {
            var val = $(this).val().trim();
            val = val.replace(/\s+/g, '');

            $('#catalogue').empty();
            if (val.length > 2) {
                $.post('Portail.php', {
                    func: 'search',
                    text: val
                }, callbackEnchere);
            } else {
                updateBiens();
            }
        });
    });
})(jQuery);

function callbackFormAdminCate(data) {
    var data = JSON.parse(data);
    switch (data.code) {
        case 0:
            $.post('Portail.php', { func: 'AdminCate' }, callbackAdminCate);
            M.toast({ html: data.msg });
            break;

        default:
            M.toast({ html: data.msg });
            break;
    }
}

function callbackFormAdminUser(data) {
    var data = JSON.parse(data);
    switch (data.code) {
        case 0:
            $.post('Portail.php', { func: 'AdminUsers' }, callbackAdminUsers);
            M.toast({ html: data.msg });
            break;

        default:
            M.toast({ html: data.msg });
            break;
    }
}

function callbackFormAdminBien(data) {
    var data = JSON.parse(data);
    switch (data.code) {
        case 0:
            $.post('Portail.php', { func: 'AdminBiens' }, callbackAdminBiens);
            M.toast({ html: data.msg });
            updateBiens();
            break;

        default:
            M.toast({ html: data.msg });
            break;
    }
}


//Callback du formulaire de connexion
function callbackConnexion(data) {
    data = JSON.parse(data);
    switch (data.code) {
        case 0: // Ok
            M.Modal.getInstance($('#modal1-connexion')).close();
            M.toast({ html: 'Bienvenue ' + data.pseudo + ' !' });
            $('#helper-id-connexion').html('').removeClass('error-text');
            $('#helper-mdp-connexion').html('').removeClass('error-text');
            refreshNavbar();
            break;
        case 1: // Identifiant introuvable
            $('#helper-id-connexion').html(data.msg).addClass('error-text');
            break;
        case 2: // Mauvais mdp
            $('#helper-mdp-connexion').html(data.msg).addClass('error-text');
            break;
        default: //Erreur
            M.toast({ html: 'Une erreure s\'est produite !' });
            break;
    }
}

//Callback du formulaire dde modification du compte
function callbackMonCompte(data) {
    data = JSON.parse(data);
    switch (data.code) {
        case 1: // Aucune modification
            M.toast({ html: 'Aucune modification !' });
            break;
        case 4: //ancien mdp incorrect
            $('#helper-amdp-mon-compte').html(data.msg).addClass('error-text');
            break;
        case 6: //nouveau mdp incorrect
            $('#helper-nmdp-mon-compte').html(data.msg).addClass('error-text');
            break;
        case 2: // ok changement pseudo
        case 3: // ok changement mdp
            data.msgs.forEach(function (el) {
                M.toast({ html: el });
            });
            M.Modal.getInstance($('#modal1-mon-compte')).close();
            refreshMonCompte();
            refreshNavbar();
            break;
        case 5: // ko pseudo
        case 7: // ko pseudo est un mail
            $('#helper-pseudo-mon-compte').html(data.msg).addClass('error-text');
            break;
        default:
            M.toast({ html: 'Une erreure s\'est produite !' });
            break;
    }
}

//Rafraichit la barre de navigation AJAX
function refreshNavbar() {
    $.post('Portail.php', { func: 'getNavbar' }, function (data) {
        $('#navbar').html(data);
        $('.dropdown-trigger').dropdown();
    });
}

//Rafraichit le formulaire de gestion du compte AJAX
function refreshMonCompte() {
    $.post('Portail.php', { func: 'getMonCompteForm' }, function (data) {
        $('#modal1-mon-compte').html(data);
        M.updateTextFields();
    });
}

// Rafraichit les formulaires d'administration
function refreshAdministrationBiens() {
    $.post('Portail.php', { func: 'getAdministrationBiens' }, callbackAdminBiens);
}
function refreshAdministrationUsers() {
    $.post('Portail.php', { func: 'getAdministrationUsers' }, callbackAdminUsers);
}
function refreshAdministrationCategories() {
    $.post('Portail.php', { func: 'getAdministrationCate' }, callbackAdminCate);
}

//Callback du formulaire d'inscription
function callbackInscription(data) {
    data = JSON.parse(data);
    switch (data.code) {
        case 0: // ok
            M.Modal.getInstance($('#modal1-connexion')).close();
            M.toast({ html: data.msg });
            refreshNavbar();
            refreshMonCompte();
            break;
        case 2: // ko pseudo deja utilisé
        case 6: // ko pseudo est un mail
            $('#helper-pseudo-inscription').html(data.msg).addClass('error-text');
            break;
        case 3: // ko email deja utilisé
        case 4: // Mail invalide
            $('#helper-mail-inscription').html(data.msg).addClass('error-text');
            break;
        case 5: //Mdp invalide
            $('#helper-mdp-inscription').html(data.msg).addClass('error-text');
            break;

        default:
            M.toast({ html: data.msg });
            break;
    }
}

function htmlEncode(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}

// Callback enchere
function callbackEnchere(data) {
    encheres = JSON.parse(data);

    encheres.forEach(function (enchere) {
        // Construction colonne libellé
        var titreEnchere = $('<h5>', { html: htmlEncode(enchere.libelle) });
        var colTitreEnchere = $('<div>', { class: 'col s3 center-align', id: 'enchere-libelle' });
        colTitreEnchere.append(titreEnchere);

        // Construction colonne desc
        var descriptionEnchere = $('<span>', { html: htmlEncode(enchere.description) });
        var colDescription = $('<div>', { class: 'col s6 center-align', id: 'enchere-description' });
        colDescription.append(descriptionEnchere);

        // Construction colonne prix
        var prix = $('<span>', { html: enchere.prix_courant + '€' });
        var colPrix = $('<div>', { class: 'col s2 center-align', id: 'enchere-prix' });
        colPrix.append(prix);

        // Construction countdown
        var containerClock = $('<div>').css('margin', '11px auto');
        var clockNum = 'clock' + enchere.id;
        var divTime = $('<div>', { class: 'col s12 center-align', id: clockNum });
        var rowTime = $('<div>', { class: 'row valign-wrapper' });
        containerClock.append(divTime);
        rowTime.append(containerClock);

        var endDate = new Date(enchere.duree);
        var startDate = new Date();
        var diff = (endDate.getTime() - startDate.getTime()) / 1000;
        var divAdd;
        if (diff > 0) {
            // Construction bouton encherir
            var iconAdd = $('<i>', { class: 'material-icons', html: 'add' });
            var lienAdd = $('<a>', { class: 'modal-trigger btn-large waves-effect red waves-light' }).attr('data-target', 'modale-encherir').attr('data-id', enchere.id);
            lienAdd.append(iconAdd);
            divAdd = $('<div>', { class: 'col s1 center-align' });
            divAdd.append(lienAdd);
        } else {
            var chip = $('<div>', { class: 'chip red white-text', html: 'Vous n\'avez pas été suffisament rapide !' });
            divTime.append(chip);
        }

        // Construction ligne
        var rowEnchere = $('<div>', { class: 'row valign-wrapper' });
        rowEnchere.append(colTitreEnchere);
        rowEnchere.append(colDescription);
        rowEnchere.append(colPrix);
        rowEnchere.append(divAdd);

        // Construction elem final
        var mainDiv = $('<div>', { class: 'card-panel horizontal grey lighten-5 z-depth-1 hoverable' });
        var content = $('<div>', { class: 'card-content' });
        mainDiv.append(content);
        content.append(rowEnchere);
        content.append(rowTime);
        $('#catalogue').append(mainDiv);

        if (diff > 0) {
            $('#' + clockNum).FlipClock(diff, {
                clockFace: 'DailyCounter',
                countdown: true,
                language: 'french',
                callbacks: {
                    stop: function () {
                        divTime.html('');
                        var chip = $('<div>', { class: 'chip red white-text', html: 'Vous n\'avez pas été suffisament rapide !' });
                        divTime.append(chip);
                        divAdd.html('');
                        $(this)[0].factory.$wrapper.removeClass('flip-clock-wrapper');

                    }
                }
            });
        }
    });
}


//Met a jour la liste des biens AJAX
function updateBiens() {
    $.post('Portail.php', {
        func: 'encheres',
        categorie: $('#filtre-categorie').val(),
        confidentialite: $('#filtre-confidentialite').val(),
        mes_encheres: $('#filtre-mes-encheres').prop('checked'),
        deja_encheri: $('#filtre-deja-encheri').prop('checked'),
        seulement_en_cours: $('#filtre-seulement-en-cours').prop('checked'),
        tri: $('#selecteur-tri').val()
    }, function (data) {
        $('#catalogue').empty();
        callbackEnchere(data);
    });
}


// Gere l'ajout d'un bien
function callbackAjoutBien(data) {
    data = JSON.parse(data);
    switch (data.code) {
        case 0: // OK
            M.Modal.getInstance($('#modal-ajout-bien')).close();
            M.toast({ html: data.msg });
            updateBiens();
            break;
        case 3:
            $('#helper-ajout-bien-date').html(data.msg).addClass('error-text');
        case 4:
            $('#helper-ajout-bien-prix').html(data.msg).addClass('error-text');
            $('#helper-ajout-bien-reserve').html(data.msg).addClass('error-text');

        default:
            break;
    }
}

function callbakEncherir(data) {
    data = JSON.parse(data);
    switch (data.code) {
        case 0:
            M.Modal.getInstance($('#modale-encherir')).close();
            M.toast({ html: data.msg });
            updateBiens();
            break;
        default:
            $('#helper-encherir').html(data.msg).addClass('error-text');
            break;
    }
}

// Met a jour la liste des categories AJAX
function updateCategories() {
    $.post('Portail.php', { func: 'categories' }, function (data) {
        $('#filtre-categorie').html(data);
        $('#filtre-categorie').formSelect();
    });
}

function callbackAdminBiens(data) {
    data = JSON.parse(data);
    data = data.msg;

    var racine = $('#modale-admin-biens').children().first().children().get(1);
    $(racine).html('');

    data.forEach(function (bien) {
        var idCol = '<div class="col s2 center-align">' + bien.id + '</div>';
        var libCol = '<div class="col s4 center-align">' + bien.libelle + '</div>';
        var selectCol = '<div class="col s3 center-align"><div class="input-field"><select id="admin-update-bien-select-id-' + bien.id + '">';
        var etats = ['accepte', 'attente', 'refuse'];
        etats.forEach(function (etat) {
            var isSelected = etat == bien.etat ? 'selected ' : '';
            var option = '<option ' + isSelected + 'value="' + etat + '">' + etat + '</option>';
            selectCol += option;
        });
        selectCol += '<label>Choix de l\'etat</label></select></div></div>';
        var button = '<div class="col s3 center-align"><a id="admin-update-bien" data-id="' + bien.id + '" class="waves-effect waves-light orange btn-small">Modifier</a></div>';

        var line = '<div class="divider"></div><div class="row valign-wrapper">' + idCol + libCol + selectCol + button + '</div>';

        $(racine).append(line);
    });

    $('select').formSelect();
}

function callbackAdminUsers(data) {
    data = JSON.parse(data);
    data = data.msg

    var racine = $('#modale-admin-users').children().first().children().get(1);
    $(racine).html('');

    data.forEach(function (user) {
        var idCol = '<div class="col s3 center-align">' + user.id + '</div>';
        var pseudoCol = '<div class="col s3 center-align">' + user.pseudo + '</div>';
        var selectCol = '<div class="col s3 center-align"><div class="input-field"><select id="admin-update-user-select-id-' + user.id + '">';
        var statuts = ['invite', 'user', 'admin'];
        statuts.forEach(function (elStatut) {
            var isSelected = (elStatut == user.statut) ? 'selected ' : '';
            var option = '<option ' + (isSelected) + 'value="' + elStatut + '">' + elStatut + '</option>';
            selectCol += option;
        });
        selectCol += '<label>Choix du statut</label></select></div></div>';
        var button = '<div class="col s3 center-align"><a id="admin-update-user" data-id="' + user.id + '" class="waves-effect waves-light orange btn-small">Modifier</a></div>';

        var user = '<div class="divider"></div><div id="admin-update-bien-id-' + user.id + '" class="row valign-wrapper">' + idCol + pseudoCol + selectCol + button + '</div>';
        $(racine).append(user);
    });
    $('select').formSelect();
}

//Callback retour formulaire
function callbackAdminCate(data) {
    data = JSON.parse(data);
    data = data.msg;

    var racine = $('#modale-admin-cate').children().first().children().get(1);
    $(racine).html('');

    data.forEach(function (cat) {
        var idCol = '<div class="col s2 center-align">' + cat.id + '</div>';
        var libCol = '<div class="col s3 center-align"><input id="admin-update-cate-id-' + cat.id + '" type="text" required value="' + cat.libelle + '"></input></div>';
        var btnUpdate = '<div class="col s3 center-align"><a id="admin-update-cate" data-id="' + cat.id + '" class="waves-effect waves-light orange btn-small">Update</a></div>'
        var btnDelete = '<div class="col s3 center-align"><a id="admin-delete-cate" data-id="' + cat.id + '" class="waves-effect waves-light red btn-small">Delete</a></div>'
        categorie

        var categorie = '<div class="divider"></div><div class="row valign-wrapper">' + idCol + libCol + btnUpdate + btnDelete + '</div>';

        $(racine).append(categorie);
    });

    var footer = '<div class="modal-footer"><form class="row valign-wrapper">';
    footer += '<div class="col s9 input-field"><input id="admin-update-cat-input" type="text" required></input>';
    footer += '<label for="admin-update-cat-input">Nom de la catégorie à ajouter</label></div>';
    footer += '<div class="col s3"><a id="admin-add-cate" class="waves-effect waves-light orange btn-small">Ajouter</a></div>';
    footer += '</form></div>';
    $(racine).append(footer);

    M.updateTextFields();
}

function callbackUpdateAdminUser(data) {
    data = JSON.parse(data);
    console.log(data);
}

function init() {
    $('.sidenav').sidenav();
    $('.modal').modal();
    $('.collapsible').collapsible();
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        firstDay: 1,
        autoClose: true,
        i18n: {
            cancel: 'Retour',
            clear: 'Effacer',
            done: 'Ok',
            months: [
                'Janvier',
                'Février',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juillet',
                'Aout',
                'Septembre',
                'Octobre',
                'Novembre',
                'Decembre'
            ],
            monthsShort: [
                'Janv',
                'Fevr',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juil',
                'Aout',
                'Sept',
                'Oct',
                'Nov',
                'Dec'
            ],
            weekdays: [
                'Dimanche',
                'Lundi',
                'Mardi',
                'Mercredi',
                'Jeudi',
                'Vendredi',
                'Samedi'
            ],
            weekdaysShort: [
                'Dim',
                'Lun',
                'Mar',
                'Mer',
                'Jeu',
                'Ven',
                'Sam'
            ], weekdaysAbbrev: ['D', 'L', 'Ma', 'Me', 'J', 'V', 'S']
        }
    });
    $('.timepicker').timepicker({
        twelveHour: false,
        autoClose: true,
        defaultTime: 'now',
        fromNow: 3600000, //Ajoute 1h
        i18n: {
            cancel: 'Retour',
            clear: 'Effacer',
            done: 'Ok',
            format: 'hh:mm'
        }
    });
    $('.materialboxed').materialbox();
}
