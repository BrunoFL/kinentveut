<?php
namespace UniTestor {
    require_once ('RequeteAjax.php');

    class ModificationUtilisateurs extends \RequeteAjax{
        private $statut;
        private $id_user;

        public function __construct($utils)
        {
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        public function modifUserStatut()
        {
            return $this->util->modifUserStatut(array($this->statut, $this->id_user));

        }

        public function modifUtilisateur()
        {
            $this->statut = $_POST['statut'];
            $this->id_user = intval($_POST['id_user']);

            //l'utilisateur doit être un admin
            if ($_SESSION['statut'] != 'admin') {
                $this->return['code'] = 2;
                $this->return['msg'] = 'L utilisateur n est pas un administrateur';
                return;
            }
            
            
            $res = $this->modifUserStatut();
            
            if ($res == 1){
                $this->return['code'] = 0;
                $this->return['msg'] = 'Utilisateur modifié';
            }

            return;
        }

    }
}