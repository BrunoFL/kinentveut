<?php

namespace UniTestor{
    require_once ('UtilsDB.php');

    class Recherche{
        private $util;
        private $return;

        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        private function getParams(){
            $this->text = $_POST['text'];
        }

        public function getCodeReturn(){
            return $this->return['code'];
        }

        public function getMsgReturn(){
            return $this->return['msg'];
        }

        public function getReturn(){
            return $this->return;
        }

        public function getReturnJSON(){
            return json_encode($this->return);
        }

        public function recherche(){
          $this->getParams();
          $this->return = $this->util->getEnchereRecherche(array($this->text));
        }
    }
}
