<?php
//Gere l'ajout d'une enchere
namespace UniTestor{
    require_once ('RequeteAjax.php');

    class AjoutEnchere extends \RequeteAjax{
        private $pseudo;
        private $user;
        private $id;
        private $userID;
        private $prix_enchere;
        
        public function __construct($utils){
            $this->return = array();
            $this->return['code'] = 1;
            $this->return['msg'] = "Erreur";
            $this->util = $utils;
        }

        private function getParams(){
            $this->pseudo = (isset($_SESSION['pseudo'])) ? $_SESSION['pseudo'] : "invite";
            $this->userID = $_SESSION['userID'];
            $this->id = intval($_POST['id']);
            $this->prix_enchere = floatval($_POST['val']);
        }

        private function dateIsTooLate(){
            $dateNow = new \DateTime("now");
            $bien = $this->getBien();
            $duree = new \DateTime($bien['duree']);
            return ($duree <= $dateNow);
        }
        
        private function priceIsTooLow(){
            $montant_actuel = $this->getLastEnchere();
            return ($this->prix_enchere <= $montant_actuel['prix_courant']);
        }

        public function getBien(){
            return $this->util->getBienById(array($this->id));
        }
        
        public function getLastEnchere(){
            return $this->util->getLastEnchereById(array($this->id));
        }

        public function addEnchere(){
            return $this->util->addEnchere(array($this->id, $this->userID, $this->prix_enchere));
        }

        public function encherir(){
            $this->getParams();

            if ($this->dateIsTooLate()){
                $this->return['code'] = 2;
                $this->return['msg'] = 'Enchere termine';
                return;
            }

            if ($this->priceIsTooLow()){
                $this->return['code'] = 3;
                $this->return['msg'] = 'Enchere trop faible';
                return;
            }

            $res = $this->addEnchere();
            if ($res == 1){
                $this->return['code'] = 0;
                $this->return['msg'] = 'Enchere prise en compte';
                return;
            }
        }
    }
}
