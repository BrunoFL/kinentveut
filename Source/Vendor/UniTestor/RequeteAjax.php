<?php
abstract class RequeteAjax{
    protected $util;
    protected $return;

    public function getCodeReturn(){
        return $this->return['code'];
    }

    public function getMsgReturn(){
        return $this->return['msg'];
    }
    
    public function getReturn(){
        return $this->return;
    }

    public function getReturnJSON(){
        return json_encode($this->return);
    }
}