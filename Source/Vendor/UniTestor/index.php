<!DOCTYPE html>
<html lang="fr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>Qui n'en veut</title>

    <!-- CSS  -->
    <link rel="stylesheet" href="css/flipclock.css">
    <link href="css/icon.css" rel="stylesheet">
    <link rel="stylesheet" href="css/materialize.min.css">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link rel="icon" type="image/png" href="assets/img/unicorn.gif" />
</head>

<body>

    <!-- Barre de navigation -->
    <div id="navbar" class="navbar-fixed">
    </div>

    <!--Fenetre modale inscription/connexion-->
    <div id="modal1-connexion" class="modal">
        <div class="modal-content">
            <ul class="collapsible">
                <li class="active">
                    <!--Connexion-->
                    <div class="collapsible-header">
                        <h4 class="center-align row">Connexion</h4>
                    </div>
                    <div class="collapsible-body">
                        <form id="form-connexion">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input required id="identifiant-connexion" type="text">
                                    <label for="identifiant-connexion">Pseudo ou Mail</label>
                                    <span id="helper-id-connexion" class="helper-text"></span>
                                </div>
                                <div class="input-field col s6">
                                    <input required id="password-connexion" type="password" class="validate">
                                    <label for="password-connexion">Mot de passe</label>
                                    <span id="helper-mdp-connexion" class="helper-text"></span>
                                </div>
                            </div>
                            <button id="submit-connexion" class="btn waves-effect waves-light" type="submit">Connexion
                                <i class="material-icons">send</i></button>
                        </form>
                    </div>
                </li>
                <li>
                    <!--Inscription-->
                    <div class="collapsible-header">
                        <h4 class="center-align row">Inscription</h4>
                    </div>
                    <div class="collapsible-body">
                        <form id="form-inscription">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input required id="email-inscription" type="email" class="validate">
                                    <label for="email-inscription">Mail</label>
                                    <span id="helper-mail-inscription" class="helper-text"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input required id="pseudo-inscription" type="text">
                                    <label for="pseudo-inscription">Pseudo</label>
                                    <span id="helper-pseudo-inscription" class="helper-text"></span>
                                </div>
                                <div class="input-field col s6">
                                    <input required id="password-inscription" type="password" class="validate">
                                    <label for="password-inscription">Mot de passe</label>
                                    <span id="helper-mdp-inscription" class="helper-text"></span>
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light" type="submit" name="action">Inscription <i
                                    class="material-icons">send</i></button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- Fenetre modale gestion du compte -->
    <div id="modal1-mon-compte" class="modal">

    </div>

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <h1 class="header center orange-text">Vente aux enchères de biens</h1>
            <div class="row center">
                <h5 class="header col s12 light">Vends tes biens comme une licorne qui dabe</h5>
                <img class="responsive-img" src="assets/img/unicorn.gif" alt="unicorn">
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="section">

<div class="row">

            <!--   Le marketplace  -->
            <div class="row">

                <div id="edition" class="col s3 grey lighten-3 hoverable">
                    <h3>Recherche</h3>
                    <div class="input-field col s12">
                        <input id="recherche-text" name="recherche-text" type="text">
                    </div>

                    <h3>Filtres</h3>

                    <!-- Filtre categorie -->
                    <div class="input-field col s12">
                        <select multiple name="filtre-categorie" id="filtre-categorie">
                        </select>
                        <label for="filtre-categorie">Catégories</label>
                    </div>

                    <!-- Filtre Confidentialité -->
                    <div class="input-field col s12">
                        <select name="filtre-confidentialite" id="filtre-confidentialite">
                            <option selected value="public">Publique</option>
                            <option value="privee">Privée</option>
                            <option value="confidentielle">Confidentielle</option>
                        </select>
                        <label for="filtre-confidentialite">Confidentialité</label>
                    </div>

                    <!-- Filtre mes encheres -->
                    <div class="input-field col s12">
                        <div class="switch">
                            <label for="filtre-mes-encheres">Mes biens uniquement : </label>
                            <label>
                                Non
                                <input id="filtre-mes-encheres" type="checkbox">
                                <span class="lever"></span>
                                Oui
                            </label>
                        </div>
                    </div>

                    <!-- Filtre deja encheri -->
                    <div class="input-field col s12">
                        <div class="switch">
                            <label for="filtre-deja-encheri">Déjà encheris uniquement : </label>
                            <label>
                                Non
                                <input id="filtre-deja-encheri" type="checkbox">
                                <span class="lever"></span>
                                Oui
                            </label>
                        </div>
                    </div>

                    <!-- Filtre statut -->
                    <div class="input-field col s12">
                        <div class="switch">
                            <label for="filtre-seulement-en-cours">Uniquement enchères : </label>
                            <label>
                                terminées
                                <input id="filtre-seulement-en-cours" checked type="checkbox">
                                <span class="lever"></span>
                                En cours
                            </label>
                        </div>
                    </div>

                    <h3>Tris</h3>
                    <div class="input-field col s12">
                        <select name="selecteur-tri" id="selecteur-tri">
                            <optgroup label="Par prix">
                                <option selected value="tri-croissant">Ordre croissant</option>
                                <option value="tri-decroissant">Ordre décroissant</option>
                            </optgroup>
                            <optgroup label="Par temps restant">
                                <option value="temps">Temps restant</option>
                            </optgroup>
                        </select>
                    </div>

                </div>

                <!-- Les biens -->
                <div class="col s9" id="catalogue">
                </div>
            </div>

        </div>
    </div>

    <!-- Ajout encheres  -->
    <div class="fixed-action-btn">
        <a id="add_bien_form" class="btn-floating btn-large orange waves-effect waves-light btn modal-trigger pulse" data-target="modal-ajout-bien">
            <i class="large material-icons">add</i>
        </a>
    </div>
    <div id="modal-ajout-bien" class="modal">
        <div class="modal-content">
            <div class="row">
                <form id="form-ajout-bien" class="col s12">
                    <h3>Ajouter un bien à mettre aux encheres</h3>
                    <div class="row">
                        <div class="input-field col s6">
                            <input required id="ajout-bien-prix-base" type="number" step="0.01" class="validate">
                            <label for="ajout-bien-prix-base">Prix de base</label>
                            <span id="helper-ajout-bien-base" class="helper-text"></span>
                        </div>
                        <div class="input-field col s6">
                            <input required id="ajout-bien-prix-reserve" type="number" step="0.01" class="validate">
                            <label for="ajout-bien-prix-reserve">Prix de réserve</label>
                            <span id="helper-ajout-bien-reserve" class="helper-text"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input required id="ajout-bien-libelle" type="text" class="validate">
                            <label for="ajout-bien-libelle">Libelle</label>
                        </div>
                        <div class="input-field col s6">
                            <select required name="ajout-bien-filtre-confidentialite" id="ajout-bien-filtre-confidentialite">
                                <option selected value="public">Public</option>
                                <option value="privee">Privé</option>
                                <option value="confidentielle">Confidentiel</option>
                            </select>
                            <label for="ajout-bien-filtre-confidentialite">Confidentialité</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input required id="ajout-bien-date" type="text" class="datepicker">
                            <label for="ajout-bien-date">Date de fin</label>
                            <span id="helper-ajout-bien-date" class="helper-text"></span>
                        </div>
                            <div class="input-field col s6">
                            <input required id="ajout-bien-heure" type="text" class="timepicker">
                            <label for="ajout-bien-heure">Heure de fin</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="ajout-bien-description" placeholder="Mon produit c'est le meilleur" class="materialize-textarea"></textarea>
                            <label for="ajout-bien-description">Description</label>
                        </div>
                    </div>
                    <button id="submit-ajout-bien" class="row waves-effect waves-light btn red center" type="submit"><i class="material-icons right">send</i>Vendre</button>
                </form>
            </div>
        </div>
    </div>

    <div id="modale-encherir" class="modal">
        <div class="modal-content">
            <div class="row">
                <h5>Encherir <span id="enchere-name"></span><span id="enchere-id"></span></h5>
            </div>
            <div class="row">
                <form id="form-encherir" class="col s12">
                    <div class="input-field col s6">
                        <input required id="enchere-valeur" type="number" step="0.01" class="validate">
                        <label for="enchere-valeur">Miser</label>
                        <span id="helper-encherir" class="helper-text"></span>
                    </div>
                    <button id="submit-encherir" class="col s6 row waves-effect waves-light btn red" type="submit"><i class="material-icons right">send</i>Enchérir</button>
                </form>
            </div>
        </div>
    </div>

    <div id="modales-administration">
        <div id="modale-admin-biens" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Gestion des biens</h4>
                <div></div>
            </div>
        </div>

        <div id="modale-admin-users" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Gestion des utilisateurs</h4>
                <div></div>
            </div>
        </div>

        <div id="modale-admin-cate" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Gestion des catégories</h4>
                <div></div>
            </div>
        </div>
    </div>

    <!--  Scripts-->
        <script src="js/jquery.min.js"></script>
    <!--
    <script src="js/jquery.countdown.min.js"></script>
-->
    <script src="js/materialize.min.js"></script>
    <script src="js/flipclock.js"></script>
    <script src="js/init.js"></script>

</body>

</html>
