<?php
//Envoie le formulaire de modification du compte
//Et gere le formulaire

function getMonCompteForm(){
    echo '<div class="modal-content">'. //var_dump($_SESSION).
    '<h4>Modification du compte</h4>'.
    '<form id="form-mon-compte">'.
    '<div class="row">'.
    
    '<div class="input-field col s6">'.
    '<input required value="'.htmlspecialchars($_SESSION['pseudo']).'" class="active" id="pseudo-mon-compte" type="text">'.
    '<label for="pseudo-mon-compte">Votre pseudo</label>'.
    '<span id="helper-pseudo-mon-compte" class="helper-text"></span>'.
    '</div>'.
    
    '<div class="input-field col s6">'.
    '<input value="'.htmlspecialchars($_SESSION['email']).'" class="active" disabled id="mail-mon-compte" type="email">'.
    '<label for="mail-mon-compte">Votre mail</label>'.
    '</div></div>'.
    '<div class="divider"></div>'.
    '<div class="row">'.
    '<div class="input-field col s6">'.
    '<input required id="amdp-mon-compte" type="password" placeholder="Pour validation">'.
    '<label for="amdp-mon-compte">Ancien mot de passe</label>'.
    '<span id="helper-amdp-mon-compte" class="helper-text"></span>'.
    '</div>'.
    '<div class="input-field col s6">'.
    '<input id="nmdp-mon-compte" placeholder="Uniquement en cas de changement" type="password">'.
    '<label for="nmdp-mon-compte">Votre nouveau mot de passe</label>'.
    '<span id="helper-nmdp-mon-compte" class="helper-text"></span>'.
    '</div></div>'.
    
    '<button id="submit-mon-compte" class="btn waves-effect waves-light" type="submit">Modifier <i class="material-icons">send</i></button>'.
    '</form></div>';   
}