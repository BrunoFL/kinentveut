-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 25 sep. 2018 à 19:42
-- Version du serveur :  10.2.14-MariaDB
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `m2test6`
--

-- --------------------------------------------------------

--
-- Structure de la table `bien`
--

DROP TABLE IF EXISTS `bien`;
CREATE TABLE IF NOT EXISTS `bien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `visibilite` enum('public','privee','confidentielle') DEFAULT NULL,
  `etat` enum('attente','accepte','refuse') DEFAULT NULL,
  `prix_base` decimal(11,2) NOT NULL,
  `prix_reserve` decimal(11,2) NOT NULL,
  `date_parution` datetime NOT NULL,
  `duree` datetime NOT NULL,
  `categorie` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `categorie` (`categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `bien`
--

INSERT INTO `bien` (`id`, `id_user`, `libelle`, `description`, `visibilite`, `etat`, `prix_base`, `prix_reserve`, `date_parution`, `duree`, `categorie`) VALUES
(1, 3, 'Stylo', 'Stylo de luxe d\'occasion !\r\nJe vous aime <3', 'confidentielle', 'accepte', '2.00', '0.00', '2018-09-22 00:00:00', '2018-09-22 00:30:00', 2),
(2, 2, 'Twingo', 'Twingo pas chère !\r\nWin dis :3', 'privee', 'accepte', '500.00', '1000.00', '2018-09-22 00:00:00', '2018-10-22 00:00:05', 3),
(3, 2, 'Hélicoptère de combat', 'Authentique faux hélicoptère de combat miniaturisé à l\'échelle 1/1000000000 ', 'public', 'accepte', '1500.00', '5000.00', '2018-09-24 00:00:00', '2018-11-07 20:22:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'Autre'),
(2, 'Accessoires'),
(3, 'Voiture');

-- --------------------------------------------------------

--
-- Structure de la table `enchere`
--

DROP TABLE IF EXISTS `enchere`;
CREATE TABLE IF NOT EXISTS `enchere` (
  `id_bien` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `montant` float NOT NULL,
  PRIMARY KEY (`id_user`,`id_bien`,`montant`),
  KEY `id_bien` (`id_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `enchere`
--

INSERT INTO `enchere` (`id_bien`, `id_user`, `montant`) VALUES
(2, 2, 550);

-- --------------------------------------------------------

--
-- Structure de la table `liste_confidentielle`
--

DROP TABLE IF EXISTS `liste_confidentielle`;
CREATE TABLE IF NOT EXISTS `liste_confidentielle` (
  `id_bien` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_bien`),
  KEY `id_bien` (`id_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `liste_confidentielle`
--

INSERT INTO `liste_confidentielle` (`id_bien`, `id_user`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statut` enum('admin','user','invite') DEFAULT NULL,
  `pseudo` varchar(100) DEFAULT NULL,
  `psswd` varchar(256) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `statut`, `pseudo`, `psswd`, `email`) VALUES
(1, 'admin', 'admin', '$2y$10$FzM66WTKWZ3IIqh0zR4foeMaiSgrFGbKMWBfkm/wayQ2wTgg1OAqa', 'admin@admin.com'),
(2, 'user', 'Toto', '$2y$10$SEl1Jd2boI0WlFJ8qI.x9.FQwWcnlosqIrReWIptRJsA.gPJhCIpO', 'toto@toto.com'),
(3, 'user', 'Ami', '$2y$10$ad31UpJVYu2ttUc5s7EhT.Euf.kx84eP/8OxAJompRMpICD62OkkW', 'ami@ami.com'),
(4, 'invite', 'invite', NULL, 'invite@invite.com');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `bien`
--
ALTER TABLE `bien`
  ADD CONSTRAINT `BIEN_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `BIEN_ibfk_2` FOREIGN KEY (`categorie`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `enchere`
--
ALTER TABLE `enchere`
  ADD CONSTRAINT `ENCHERE_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `ENCHERE_ibfk_2` FOREIGN KEY (`id_bien`) REFERENCES `bien` (`id`);

--
-- Contraintes pour la table `liste_confidentielle`
--
ALTER TABLE `liste_confidentielle`
  ADD CONSTRAINT `LISTE_CONFIDENTIELLE_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `LISTE_CONFIDENTIELLE_ibfk_2` FOREIGN KEY (`id_bien`) REFERENCES `bien` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
