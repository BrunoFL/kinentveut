<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class AjoutBien extends atoum {
        

        public function testAjoutBienVide () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutBien($util);
            $this
            ->integer( $i->getCodeReturn())
                ->isEqualTo(1);
        }
        
        
        //test ok
        public function testAjoutbienOk () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutBien($util);
            $this
            ->and($_SESSION['userID'] = "1")
            ->and($_POST["libelle"] = "libelle")
            ->and($_POST["description"] = "description")
            ->and($_POST["confidentialite"] = "confidentialite")
            ->and($_POST["date"] = "2020-01-0 00:00:00")
            ->and($_POST["prix_reserve"] = 15)
            ->and($_POST["prix_base"] = 10)
            ->and($this->calling($util)->addBien = 1)
            ->if( $i->ajoutbien())
            ->integer( $i->getCodeReturn())
            ->isEqualTo(0);
        }
        
        // prix reserve inferieur à prix base 
        public function testAjoutbienKO4 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutBien($util);
            $this
            ->and($_SESSION['userID'] = "1")
            ->and($_POST["libelle"] = "libelle")
            ->and($_POST["description"] = "description")
            ->and($_POST["confidentialite"] = "confidentialite")
            ->and($_POST["date"] = "2019-09-22 00:30:00")
            ->and($_POST["prix_reserve"] = "80")
            ->and($_POST["prix_base"] = "100")
            ->and($this->calling($util)->addBien = 0)
            ->if( $i->ajoutbien())
            ->integer( $i->getCodeReturn())
            ->isEqualTo(4);
        }
        
        // Date fin invalide  
        public function testAjoutbienKO5 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutBien($util);
            $this
            ->and($_SESSION['userID'] = "1")
            ->and($_POST["libelle"] = "libelle")
            ->and($_POST["description"] = "description")
            ->and($_POST["confidentialite"] = "confidentialite")
            ->and($_POST["date"] = "2017-09-22 00:30:00")
            ->and($_POST["prix_reserve"] = "prix_reserve")
            ->and($_POST["prix_base"] = "prix_base")
            ->if( $i->ajoutbien())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(3);
        }
    }
}