<?php
/**
 * Created by PhpStorm.
 * User: yassine
 * Date: 16/10/18
 * Time: 18:04
 */

namespace UniTestor\tests\units {

    use \atoum;

    class ModificationUtilisateurs extends atoum
    {


        public function testUtilisateurModificationVide()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationUtilisateurs($util);
            $this
                ->integer($i->getCodeReturn())
                ->isEqualTo(1);
        }


        //test ok
        public function testModificationUtilisateurok()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationUtilisateurs($util);
            $this
                ->and($_SESSION["statut"] = "admin")
                ->and($_POST["id_user"] = "1")
                ->and($_POST["statut"] = "user")
                ->and($this->calling($util)-> modifUserStatut =1)
                ->if($i->modifUtilisateur())
                ->integer($i->getCodeReturn())
                ->isEqualTo(0);
        }

        //test ko1 : L'utilasteur n'est pas un admin
        public function testModificationUtilisateurK01()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationUtilisateurs($util);
            $this
                ->and($_SESSION["statut"] = "invite")
                ->and($_POST["id_user"] = "1")
                ->and($_POST["statut"] = "1")
                ->if($i->modifUtilisateur())
                ->integer($i->getCodeReturn())
                ->isEqualTo(2);
        }


    }

}