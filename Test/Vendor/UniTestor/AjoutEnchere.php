<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class AjoutEnchere extends atoum {
        

        public function testEnchereVide () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutEnchere($util);
            $this
            ->integer( $i->getCodeReturn())
                ->isEqualTo(1);
        }
        
        
        //test ok
        public function testEncherirOk () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutEnchere($util);
            $this
            ->and($_SESSION["userID"] = 1)
            ->and($_SESSION["pseudo"] = "toto")
            ->and($_POST["id"] = "1")
            ->and($_POST["val"] = "80.00")
            ->and($this->calling($util)->getIdByPseudo = array("id"=>"1"))
            ->and($this->calling($util)->getBienById = array("duree"=>"2020-01-01 00:00:00"))
            ->and($this->calling($util)->getLastEnchereById = array("prix_courant"=>"70.00"))
            ->and($this->calling($util)->addEnchere = 1)
            ->if( $i->encherir())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(0);
        }

        //test ko1 : enchere trop faible
        public function testEncherirK01EnchereTropFaible () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutEnchere($util);
            $this
            ->and($_SESSION["userID"] = 1)
            ->and($_SESSION["pseudo"] = "toto")
            ->and($_POST["id"] = "1")
            ->and($_POST["val"] = "80.00")
            ->and($this->calling($util)->getBienById = array("duree"=>"2020-01-01 00:00:00"))
            ->and($this->calling($util)->getLastEnchereById = array("prix_courant"=>81))
            ->and($this->calling($util)->addEnchere = 0)
            ->if( $i->encherir())
            ->integer( $i->getCodeReturn())
            ->isEqualTo(3);
        }
        
        //test ko2 : enchere egale au montant actuel
        public function testEncherirK02EnchereEgale () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutEnchere($util);
            $this
            ->and($_SESSION["userID"] = 1)
            ->and($_SESSION["pseudo"] = "toto")
            ->and($_POST["id"] = "1")
            ->and($_POST["val"] = "80.00")
            ->and($this->calling($util)->getBienById = array("duree"=>"2020-01-01 00:00:00"))
            ->and($this->calling($util)->getLastEnchereById = array("prix_courant"=>80))
            ->and($this->calling($util)->addEnchere = 0)
            ->if( $i->encherir())
            ->integer( $i->getCodeReturn())
            ->isEqualTo(3);
        }
        
        public function testEnchereDateKo(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\AjoutEnchere($util);
            $this
            ->and($_SESSION["userID"] = 1)
            ->and($_SESSION["pseudo"] = "toto")
            ->and($_POST["id"] = "1")
            ->and($_POST["val"] = "100.00")
            ->and($this->calling($util)->getBienById = array("duree"=>"2017-01-01 00:00:00
"))
            ->and($this->calling($util)->getLastEnchereById = array("montant"=>80))
            ->and($this->calling($util)->addEnchere = 0)
            ->if( $i->encherir())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(2);
        }

        //TODO : condition sur la duree de l'enchere
    }
}