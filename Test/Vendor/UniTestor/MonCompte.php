<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class MonCompte extends atoum {

        //Etat de base
        public function testConnexionVide (){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);
            $this
            ->integer( $c->getCodeReturn())
                ->isEqualTo(1);
        }

        //Chnagement mauvais mdp ko
        public function testChangementMDPKO(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = null;

            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);
            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $npseudo)
                ->given($_POST['amdp'] = "1234")
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(4);
        }

        //Changement pseudo ok
        public function testPseudoOk(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "jeannot";
            
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);
            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $npseudo)
                ->given($_POST['amdp'] = "noob")
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByPseudo = 0)
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->given($this->calling($util)->updatePseudoUserByMail = 1)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(2);            
        }

        //Changement pseudo deja prit
        public function testPseudoDejaPrit(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "jeannot";
            
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);
            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $npseudo)
                ->given($_POST['amdp'] = "noob")
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByPseudo = array("pseudo"=>"jeannot"))
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(5);
        }

        //Changement pseudo deja prit
        public function testPseudoEstMail(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "admin@admin.fr";
            
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);
            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $npseudo)
                ->given($_POST['amdp'] = "noob")
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByPseudo = array("pseudo"=>"jeannot"))
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(7);
        }

        //Changement mdp ok
        public function testChangeMDPOK(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "jeannot";
            $amdp = "mdp";
            $nmdp = "newMDP007";

            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);

            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $apseudo)
                ->given($_POST['amdp'] = $amdp)
                ->given($_POST['nmdp'] = $nmdp)
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByPseudo = 0)
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->given($this->calling($util)->updateMDPUserByMail = 1)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(3);                
        }

        //Changement mdp ko
        public function testChangeMDPKO(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "jeannot";
            $amdp = "mdp";
            $nmdp = "new mdp";

            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);

            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $apseudo)
                ->given($_POST['amdp'] = $amdp)
                ->given($_POST['nmdp'] = $nmdp)
                ->given($_SESSION['pseudo'] = $apseudo)
                ->given($this->calling($util)->getUserByPseudo = 0)
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(6);                
        }

        //changement pseudo et mdp ok
        public function testChangeMDPAndPseudoOK(){
            $mail = "mekarni@gmail.com";
            $apseudo = "mekarni";
            $npseudo = "jeannot";
            $amdp = "mdp";
            $nmdp = "newMDP007";

            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\MonCompte($util);

            $this
                ->given($_POST['mail'] = $mail)
                ->given($_POST['pseudo'] = $apseudo)
                ->given($_POST['amdp'] = $amdp)
                ->given($_POST['nmdp'] = $nmdp)
                ->given($_SESSION['pseudo'] = $npseudo)
                ->given($this->calling($util)->getUserByPseudo = 0)
                ->given($this->calling($util)->getUserByMail = array("pseudo"=>$apseudo, "email" => $mail, "psswd" => "noob", "id"=>"0"))
                ->given($this->function->password_verify = true)
                ->given($this->calling($util)->updatePseudoUserByMail = 1)
                ->given($this->calling($util)->updateMDPUserByMail = 1)
                ->if($c->updateMonCompte())
                    ->then
                        ->integer( $c->getCodeReturn())
                            ->isEqualTo(3)
                        ->integer(count( ($c->getReturn()['msgs']) ))
                            ->isEqualTo(2);   
        }


    }

}