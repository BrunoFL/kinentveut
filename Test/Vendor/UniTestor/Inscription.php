<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class Inscription extends atoum {
        
        //Etat de base
        public function testInscriptionVide () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->integer( $i->getCodeReturn())
                ->isEqualTo(1);
        }

        //Test ok
        public function testInscriptionOk () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->and($this->calling($util)->getIdByPseudo = array('id'=>'1'))
            ->and($this->calling($util)->insertUser = 1)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(0);
        }
        
        //Mail invalide
        public function testInscriptionMailKO1 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarnifr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(4);
        }
        
        public function testInscriptionMailKO2 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni.mekarni.fr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(4);
        }
        
        //Pseudo invalide (cad pas un mail)
        public function testInscriptionPseudoKO () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni@mekarni.com")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(6);
        }
            

        //Pseudo deja pris
        public function testInscriptionPseudoDejaPris () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = array("pseudo"=>"mekarni"))
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->and($this->calling($util)->insertUser = 1)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(2);
        }
        
        //Mail deja 
        public function testInscriptionMailDejaPris () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "Azerty123456")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = array("email"=>"mekarni@mekarni.fr"))
            ->and($this->calling($util)->insertUser = 1)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(3);
        }
        
        //MDP valide
        public function testInscriptionMDPKO1 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "RGE")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->and($this->calling($util)->insertUser = 1)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(5);
        }

        // Pas de maj ni chiffre
        public function testInscriptionMDPKO2 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\Inscription($util);
            $this
            ->and($_POST["mail"] = "mekarni@mekarni.fr")
            ->and($_POST["mdp"] = "pzebfriegnoerg")
            ->and($_POST["pseudo"] = "mekarni")
            ->and($this->calling($util)->getPseudoByPseudo = 0)
            ->and($this->calling($util)->getPseudoByEmail = 0)
            ->and($this->calling($util)->insertUser = 1)
            ->if( $i->inscription())
                ->integer( $i->getCodeReturn())
                    ->isEqualTo(5);
        }
    }
}
