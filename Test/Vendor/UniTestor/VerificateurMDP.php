<?php
/* Pour tester la validité des mdps,
je fais un pairwise avec 4 propriétés :
    - la taille > 8
    - possede une lettre minuscule
    - possede une lettre majuscule
    - possede un chiffre

Et j'obtiens ce tableau de pairwise
https://pairwise.teremokgames.com/4dy4/

|   | taille | maj | min | nombre | 
|---|--------|-----|-----|--------| 
| 1 | ok     | ok  | ok  | ok     | <- test ok 
| 2 | ok     | ko  | ko  | ko     | <- test ko Pair 1
| 3 | ko     | ko  | ok  | ko     | <- test ko Pair 2
| 4 | ko     | ok  | ko  | ok     | <- test ko Pair 3


*/
namespace UniTestor\tests\units{
    use \atoum;
    
    class VerificateurMDP extends atoum {
        // Ok large
        public function testVerifMDPOK1 () {
            $mdp = "ohuiojNIJNN65215";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isTrue();
        }

        // Ok pile 8
        public function testVerifMDPOK2 () {
            $mdp = "oiNJ5678";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isTrue();
        }

        //Vide
        public function testVerifMDPKO1 () {
            $mdp = "";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isFalse();
        }

        //Trop court taille = 7
        public function testVerifMDPKO2 () {
            $mdp = "aZ5aaaa";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isFalse();
        }

        //Pairwise cas 1
        public function testVerifMDPPair1 () {
            $mdp = "              ";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isFalse();
        }

        //Pairwise cas 2
        public function testVerifMDPPair2 () {
            $mdp = "kergojerngregnoj";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isFalse();
        }

        //Pairwise cas 3
        public function testVerifMDPPair3 () {
            $mdp = "KIEJ151";
            $verif = new \UniTestor\VerificateurMDP($mdp);
            $this->boolean($verif->isValid())
                ->isFalse();
        }

    }
}