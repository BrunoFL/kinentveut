<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class Categories extends atoum {
        public function testCategories1 (){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Categories($util);
            $res = array(
                ["id" => "1", "libelle" => "Autre"]
            );
            $this
            ->given($this->calling($util)->getCategories = $res)
            ->if ($c->getCategories())
            ->string( $c->getReturn())
                ->isEqualTo('<option value="1">Autre</option>');
        }

        public function testCategories2 (){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Categories($util);
            $res = array(
                ["id" => "1", "libelle" => "Autre"],
                ["id" => "2", "libelle" => "Accessoires"],
                ["id" => "3", "libelle" => "Voiture"]
            );
            $this
            ->given($this->calling($util)->getCategories = $res)
            ->if ($c->getCategories())
            ->string( $c->getReturn())
                ->isEqualTo('<option value="1">Autre</option>'.
                            '<option value="2">Accessoires</option>'.
                            '<option value="3">Voiture</option>');
        }

        public function testCategoriesVide (){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Categories($util);
            $res = array();
            $this
            ->given($this->calling($util)->getCategories = $res)
            ->if ($c->getCategories())
            ->string( $c->getReturn())
                ->isEqualTo('');
        }
    }
}