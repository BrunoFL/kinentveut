<?php

namespace UniTestor\tests\units{

    use \atoum;

    class ModificationBien extends atoum {

        public function testEnchereModificationVide(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationBien($util);
            $this
                ->integer( $i->getCodeReturn())
                ->isEqualTo(1);
        }


        //test ok
        public function testModificationok(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationBien($util);
            $this
                ->and($_SESSION["statut"]= "admin")
                ->and($_POST["id_bien"] = "1")
                ->and($_POST["etat"] = "accepte")
                ->and($this->calling($util)->editStatutEnchere = 1)
                ->if($i->modifEnchere())
                ->integer( $i->getCodeReturn())
                ->isEqualTo(0);
        }

        //test ko1 : L'utilasteur n'est pas un admin
        public function testModificationK01 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationBien($util);
            $this
                ->and($_SESSION["statut"] = "invite")
                ->and($_POST["id_bien"] = "1")
                ->if( $i->modifEnchere())
                    ->integer($i->getCodeReturn())
                        ->isEqualTo(2);
        }

    }
}

