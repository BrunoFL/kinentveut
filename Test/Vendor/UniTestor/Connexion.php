<?php
namespace UniTestor\tests\units{
    use \atoum;
    
    class Connexion extends atoum {

        //Etat de base
        public function testConnexionVide ()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Connexion($util);
            $this
            ->integer( $c->getCodeReturn())
                ->isEqualTo(1);
        }


        //Connexion ok
        public function testConnexionOk(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Connexion($util);

            $this
            ->and($_POST["identifiant"] = "mekarni")
            ->and($_POST["password"] = "mdp")
            ->and($this->calling($util)->getIdentifiantByMailOrPseudo = 
                array("pseudo"=>"mekarni", "email" => "mekarni@gmail.com", "statut" => "noob", "id"=>"0"))
            ->and($this->calling($util)->getPasswordByMailOrPseudo = "\$2y\$10\$R8zjtIqzYcxQgTXE3QBNTOII7AYtcU11TPuOBMtKK0zsx8vutNqW2")
            // ->and($this->function->password_verify = true)
            ->if($c->connexion())
                ->then
                    ->integer($c->getCodeReturn())
                        ->isEqualTo(0);
        }
        
        //retour mot de passe incorrect
        public function testConnexionPswdFalse(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Connexion($util);

            $this
            ->and($_POST["identifiant"] = "mekarni")
            ->and($_POST["password"] = "mdp")
            ->and($this->calling($util)->getIdentifiantByMailOrPseudo = array("pseudo"=>"mekarni", "email" => "mekarni@gmail.com"))
            ->and($this->calling($util)->getPasswordByMailOrPseudo = "psswd")
            ->if($c->connexion())
                ->then
                    ->integer($c->getCodeReturn())
                        ->isEqualTo(2);
        }

        // retour id introuvable
        public function testConnexionIdFalse(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Connexion($util);

            $this
            ->and($_POST["identifiant"] = "romain.mekarnj")
            ->and($_POST["password"] = "mdp")
            ->and($this->calling($util)->getIdentifiantByMailOrPseudo = array("email"=>"", "pseudo"=>""))
            ->if($c->connexion())
                ->then
                    ->integer( $c->getCodeReturn())
                        ->isEqualTo(1);
        }
    
        //le retour est celui de l'id introuvable
        public function testConnexionIdAndPswdFalse(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $c = new \UniTestor\Connexion($util);
            $this
            ->and($_POST["identifiant"] = "merni")
            ->and($_POST["password"] = "mdp2")
            ->and($this->calling($util)->getIdentifiantByMailOrPseudo = array("pseudo"=>"mekarni", "email" => "mekarni@gmail.com"))
            ->and($this->calling($util)->getPasswordByMailOrPseudo = "mdp")
            ->if($c->connexion())
                ->then
                    ->integer( $c->getCodeReturn())
                        ->isEqualTo(1);
        }
    }
}