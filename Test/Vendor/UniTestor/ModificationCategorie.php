<?php
/**
 * Created by PhpStorm.
 * User: yassine
 * Date: 20/10/18
 * Time: 17:27
 */

namespace UniTestor\tests\units {

    use \atoum;

    class ModificationCategorie extends atoum {


        public function testModificationCategorieVide(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->integer( $i->getCodeReturn())
                ->isEqualTo(1);
        }


        //test ok
        public function testModificationCategorieok(){
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"]= "admin")
                ->and ($_POST['id_cate'] = 1)
                ->and($_POST["libelle"] = "libelle")
                ->and($this->calling($util)->modifCategorie = 1)
                ->if( $i->modifCategorie())
                ->integer( $i->getCodeReturn())
                ->isEqualTo(0);

        }

        //test ko1 : L'utilasteur n'est pas un admin
        public function testModificationCategorieK01 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"] = "invite")
                ->and($_POST["libelle"] = "libelle")
                ->and($_POST["id_cate"] = "1")
                ->if( $i->modifCategorie())
                ->integer($i->getCodeReturn())
                ->isEqualTo(2);
        }


        //titre categorie n'est pas rempli
        public function testModificationCategorieK02 () {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"] = "admin")
                ->and($_POST["id_cate"] = "1")
                ->and($_POST["libelle"]=Null)
                ->if( $i->modifCategorie())
                ->integer($i->getCodeReturn())
                ->isEqualTo(5);

        }
        public function testModificationCategorieK03() {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"] = "admin")
                ->and($_POST["id_cate"] = "1")
                ->and($_POST["libelle"]="12345678901234567890123456")
                ->if( $i->modifCategorie())
                ->integer($i->getCodeReturn())
                ->isEqualTo(6);

        }

      public function testSuppressionCategorieKO()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"] = "admin")
                ->and($_POST["id_cate"] = "1")
                ->and($this->calling($util)->deleteCategorie = 0)
                ->if($i->deleteCategorie())
                ->integer($i->getCodeReturn())
                ->isEqualTo(3);
        }

        public function testSuppressionCategorieOK()
        {
            $util = new \mock\UniTestor\UtilsDB(null);
            $i = new \UniTestor\ModificationCategorie($util);
            $this
                ->and($_SESSION["statut"] = "admin")
                ->and($_POST["id_cate"] = "1")
                ->and($this->calling($util)->deleteCategorie = 1)
                ->if($i->deleteCategorie())
                ->integer($i->getCodeReturn())
                ->isEqualTo(0);
        }


    }



}